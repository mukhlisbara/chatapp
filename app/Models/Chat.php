<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * Get the room that owns the Chat
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room()
    {
        return $this->belongsTo(ChatRoom::class, 'room_id', 'id');
    }

    /**
     * Get the sender that owns the Chat
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id', 'id');
    }

    // // protected $guarded = [];

    // protected $fillable = [
    //     'sender_id',
    //     'receiver_id',
    //     'last_time_message',
    //     'conversation_id',
    //     'read',
    //     'body',
    // ];

    // public function conversation()
    // {
    //     return $this->belongsTo(Conversation::class);
    //     # code...
    // }

    // public function user()
    // {
    //     return $this->belongsTo(User::class, 'sender_id');
    //     # code...
    // }

    // public function sender()
    // {
    //     return $this->belongsTo(User::class, 'owner_id');
    // }

    // public function receiver()
    // {
    //     return $this->belongsTo(User::class, 'friend_id');
    // }
}
