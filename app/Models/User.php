<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($user) {
            $user->name = "Deleted Account";
            $user->photo = "user.png";
            $user->save();
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'photo',
        'bio',
        'email',
        'username',
        'password',
        'phone',
        'location',
        'gender',
        'email_verified_at',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // public function posts()
    // {
    //     return $this->hasMany(Post::class);
    // }

    // public function highlights()
    // {
    //     return $this->hasMany(Highlight::class);
    // }

    /**
     * Get all of the friends for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function friends()
    {
        return $this->belongsToMany(User::class, 'friends', 'user_id', 'friend_id')->withTimestamps()->withPivot(['is_asked', 'is_accepted', 'is_blocked'])->as('friend');
    }

    public function acceptedFriends()
    {
        return $this->belongsToMany(User::class, 'friends', 'user_id', 'friend_id')->withTimestamps()->withPivot(['is_asked', 'is_accepted', 'is_blocked'])->wherePivot('is_asked', 1)->wherePivot('is_accepted', 1)->as('acceptedFriend');
    }

    // Get semua user yang kita ajak berteman
    public function requestedFriends()
    {
        return $this->belongsToMany(User::class, 'friends', 'user_id', 'friend_id')->withTimestamps()->withPivot(['is_asked', 'is_accepted'])->wherePivot('is_asked', 1)->wherePivot('is_accepted', 0)->as('requestedFriend');
    }

    // Get semua orang yang mengajak berteman
    public function friendsRequested()
    {
        return $this->belongsToMany(User::class, 'friends', 'friend_id', 'user_id')->withTimestamps()->withPivot(['is_asked', 'is_accepted'])->wherePivot('is_asked', 1)->wherePivot('is_accepted', 0)->as('friendRequested');
    }

    public function participates()
    {
        return $this->hasMany(Participant::class, 'user_id');
    }

    public function rooms()
    {
        return $this->belongsToMany(ChatRoom::class, 'participants', 'user_id', 'room_id');
    }
}
