<?php

namespace App\Http\Livewire\Auth;

use App\Models\User;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;

class Register extends Component
{
    public $name, $email, $username, $password, $password_confirmation, $gender, $location, $phone;
    public function render()
    {
        return view('livewire.auth.register')->extends('layouts.app')->section('content');
    }

    public function rules()
    {
        return [
            'username'     => ['regex:/^\S*$/u', 'unique:users', 'nullable'],
            'email'        => ['email', 'unique:users', 'nullable'],
            'password'     => ['confirmed', 'nullable'],
            'phone'        => ['numeric', 'nullable']
        ];
    }

    public function registerUser()
    {
        if ($this->gender == null) {
            $this->gender = 'male';
        }

        if ($this->location == null) {
            $this->location = 'surabaya';
        }

        $this->validate([
            'name' => 'required',
            'gender' => 'required',
            'location' => 'required',
            'phone' => ['required', 'numeric'],
            'username' => ['required', 'regex:/^\S*$/u', 'unique:users'],
            'email' => ['required', 'email', 'unique:users'],
            'password' => ['required', 'confirmed'],
        ]);

        $user = User::create([
            'name' => $this->name,
            'gender' => $this->gender,
            'location' => $this->location,
            'phone' => $this->phone,
            'username' => $this->username,
            'email' => $this->email,
            'password' => bcrypt($this->password),
        ]);

        Auth::guard()->login($user);
        event(new Registered($user));
        return redirect()->route('dashboard');
    }

    public function checkInput()
    {
        $this->validate();
    }
}
