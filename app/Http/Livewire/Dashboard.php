<?php

namespace App\Http\Livewire;

use App\Http\Traits\UploadFile;
use App\Models\ChatRoom;
use App\Models\Chat;
use App\Models\Friend;
use App\Models\User;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Livewire\Component;
use Livewire\WithFileUploads;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class Dashboard extends Component
{
    use WithFileUploads, LivewireAlert;
    use UploadFile;

    public $user;
    public $tabpane = 'chat';
    public $navbar = 'chat';
    public $chat_input;
    public $users;
    // public $friend;
    public $friends;
    public $requestFriends;
    public $isFriend, $is_friend_blocked;
    public $is_request_friend, $is_friend_request;
    public $pending_requests, $friend_requests;
    public $allUser, $all;
    public $user_yg_dipilih;
    public $blacklisted, $blacklisted_ids;
    public $room, $receiver, $chat_rooms;
    public $chats = [];
    public $media_chat;
    public $search, $search_gender, $search_location;

    // for storing profile's input
    public $old_password, $new_password, $new_password_confirmation;

    // public $sedan;

    public $photo, $name, $username, $bio, $email, $phone, $location, $gender;
    public $mengajak_berteman;
    public $ada_chat_baru;

    protected $listeners = [
        'editProfile'
    ];
    public function getListeners()
    {
        return ['editProfile'];
    }

    public function initProfileData()
    {
        // $user = Auth::user();
        // dd('test');
        $this->user = User::find(Auth::id());

        $this->name     =  $this->user->name;
        $this->username =  $this->user->username;
        $this->email    =  $this->user->email;
        $this->phone    =  $this->user->phone;
        $this->photo    =  $this->user->photo;
        $this->bio      =  $this->user->bio;
        $this->gender   =  $this->user->gender;
        $this->location =  $this->user->location;
        // $this->chat_input = $this->user->chat_input;
    }

    public function gantiTab($tab)
    {
        $this->tabpane = $tab;
        $this->navbar = $tab;
        $this->reset(["search_gender", "search_location"]);
    }

    // public function updatedSedan()
    // {
    //     dd($this->sedan);
    // }

    public function checkEditedEmail()
    {
        // dd($this);
        // dd($this->validate(['email'     => 'required|email:rfc,dns']));
        $this->validate([

            'name'     => 'required',
            'username' => ['required', Rule::unique('users')->ignore($this->user->id)],
            'email'    => ['required', Rule::unique('users')->ignore($this->user->id), 'email:rfc,dns'],
            'phone'    => ['required', 'numeric'],
            'gender'   => 'required',
            'location' => 'required',
            'bio'      => 'nullable',
            'photo'    => 'nullable',

        ]);
        // dd("Yaudah");

        // Edit Email
        if ($this->user->email != $this->email) {
            $this->confirm('Before Saving...', [
                'text'                    => "System detected that you want to change your email. You will need to re-verify Your new email in order to use our app again. Are You sure?",
                'position'                => 'center',
                'toast'                   => false,
                'inputAttributes'         => ['need_verify' => 1],
                'confirmButtonColor'    => '#3085d6',
                'cancelButtonColor'        => '#d33',
                'confirmButtonText'     => 'Yes, Change It!',
                'cancelButtonText'        => 'No, Cancel It!',
                'onConfirmed'            => 'editProfile',
            ]);
        } else {
            $data['data']['inputAttributes']['need_verify'] = 0;
            $this->editProfile($data);
        }

        // $user = User::find(Auth::id());

        // $user->fill([
        //     'name'      => $this->name,
        //     'username'  => $this->username,
        //     'email'     => $this->email,
        //     'phone'     => $this->phone,
        //     'gender'     => $this->gender,
        //     // 'photo'     => $this->photo,
        //     'location'  => $this->location,
        //     'bio'       => $this->bio,

        // ]);

        // // Check if user upload a foto
        // // dd($this->name);
        // if ($this->photo && !is_string($this->photo)) {
        //     $result = $this->_uploadFile($this->photo, 'images/profile');

        //     if ($result['path']) {
        //         $user->fill([
        //             'photo' => $result['filename'],
        //         ]);
        //     }
        // }

        // if ($data['data']['inputAttributes']['need_verify']) {
        //     $user->email_verified_at = null;
        //     $user->save();
        //     $this->flash('success', 'Success!', [
        //         'text' => 'Successfully changed profile!'
        //     ], '/');
        // }

        // $user->save();

        // $this->user = $user;
        // // dd(Auth::user());

        // // Call js to close edit profile modal
        // $this->emit('closeModalEditProfile');

        // // Give success message after edit profile finish
        // $this->alert('success', 'Success!', [
        //     'text' => 'Successfully changed profile!'
        // ]);
    }

    public function editProfile($data)
    {
        // $is_name_change = $this->user->name != $this->name ? true : false;
        // $is_username_change = $this->user->username != $this->username ? true : false;
        // $is_email_change = $this->user->email != $this->email ? true : false;
        // $is_phone_change = $this->user->phone != $this->phone ? true : false;
        // $is_gender_change = $this->user->gender != $this->gender ? true : false;
        // $is_location_change = $this->user->location != $this->location ? true : false;
        // // $is_bio_change = $this->bio == null || $this->bio == '' ? false : ($this->user->bio != $this->bio ? true : false);
        // $is_bio_change = ($this->user->bio != $this->bio ? true : false);
        // $is_photo_change = $this->user->photo != $this->photo ? true : false;
        // // dd($is_name_change);
        // $is_change = $is_name_change || $is_username_change || $is_email_change || $is_phone_change || $is_gender_change || $is_location_change || $is_bio_change || $is_photo_change ? true : false;

        // if ($is_change) {

        // }

        $user = User::find($this->user->id);
        // FIll the data and edit the data in database
        $user->fill([
            'name'     => $this->name,
            'username' => $this->username,
            'email'    => $this->email,
            'phone'    => $this->phone,
            'gender'   => $this->gender,
            'location' => $this->location,
            'bio'      => $this->bio,
        ]);

        // Check if user upload a foto
        // dd($this->name);
        if ($this->photo && !is_string($this->photo)) {
            $result = $this->_uploadFile($this->photo, 'images/profile');

            if ($result['path']) {
                $user->fill([
                    'photo' => $result['filename'],
                ]);
            }
        }

        if ($data['data']['inputAttributes']['need_verify']) {
            $user->email_verified_at = null;
            $user->save();
            $this->flash('success', 'Success!', [
                'text' => 'Successfully changed profile!'
            ], '/');
        }

        $is_change = $user->isDirty();

        $user->save();

        $this->user = $user;

        // Give success message after edit profile finish
        if ($is_change) {
            $this->alert('success', 'Success!', [
                'text' => 'Successfully changed profile!'
            ]);
        }

        // Re-init the logged in user data
        $this->initProfileData();

        // Call js to close edit profile modal
        // $this->emit('closeModalEditProfile');
    }


    public function changePassword()
    {
        # Validation
        $this->validate([
            'old_password' => 'required',
            'new_password' => 'required|confirmed',
        ]);

        # Match The Old Password
        if (!Hash::check($this->old_password, auth()->user()->password)) {
            throw ValidationException::withMessages(['old_password' => "Old Password doesn't match!"]);
            // return back()->with("error", "Old Password Doesn't match!");
        }

        #Update the new Password
        User::whereId(auth()->user()->id)->update([
            'password' => Hash::make($this->new_password)
        ]);

        // Give success message after change password finish
        $this->alert('success', 'Success!', [
            'text' => 'Successfully changed password!'
        ]);

        // reset input change password form data
        $this->reset(['old_password', 'new_password', 'new_password_confirmation']);

        // Call js to close change password modal
        $this->emit('closeChangePasswordModal');
    }

    public function removePhotoProfile()
    {
        $this->user->photo = 'user.png';
        $this->user->save();

        $this->alert('success', 'Success!', [
            'text' => 'Your profile photo has been successfully removed!'
        ]);

        $this->emit('changePreviewPhotoProfile');

        $this->initProfileData();
    }

    public function deleteAccount()
    {
        $user = User::find(Auth::id());

        Auth::logout();

        request()->session()->invalidate();

        request()->session()->regenerateToken();

        $user->delete();

        $this->flash(
            'success',
            'Berhasil!',
            [
                'text' => 'Profile Telah Dihapus!',
                'toast' => false,
                'position' => 'center',
            ],
            route('login')
        );
    }

    public function mount()
    {
        $this->initProfileData();
        if ($this->user->phone == null || $this->user->phone == '') {
            return redirect()->route("fill-data");
        }
        $this->user = Auth::user();
        $this->friends = Auth::user()->acceptedFriends;

        $this->refreshChat();
    }

    public function render()
    {
        // Get all user id where user is not have any relation with logged in user
        $request_friend_ids = $this->user->requestedFriends()->pluck('friend_id')->toArray();
        $friend_request_ids = $this->user->friendsRequested()->pluck('user_id')->toArray();
        $friend_ids         = $this->user->acceptedFriends()->pluck('friend_id')->toArray();
        $this->blacklisted_ids    = array_merge($request_friend_ids, $friend_request_ids, $friend_ids, [Auth::id()]);

        // Get all list user where that user doesn't have any relation with logged in user
        // $this->users = User::whereNotIn('id', $this->blacklisted_ids)->get();
        // $this->pending_requests = $this->user->requestedFriends;
        // $this->friends = $this->user->acceptedFriends;

        // friend blocked
        // if ($this->room && !$this->is_friend_blocked) {
        //     $this->room->chats()->whereNot('sender_id', Auth::id())->where('deleted_from_receiver', 0)->update([
        //         'is_seen' => 1,
        //     ]);
        // }
        $is_search_name = ($this->search && $this->search != null && $this->search != '') ? true : false;
        $is_search_gender = ($this->search_gender && $this->search_gender != null && $this->search_gender != '') ? true : false;
        $is_search_location = ($this->search_location && $this->search_location != null && $this->search_location != '') ? true : false;

        $this->user = User::where('id', Auth::id())->first();


        if ($this->room) {
            $this->room->chats()->whereNot('sender_id', Auth::id())->update([
                'is_seen' => 1,
            ]);
        }

        // Get all list chat rooms for list Chats
        $this->ada_chat_baru = 0;
        $this->chat_rooms = ChatRoom::with('participants.user')->hasUser($this->user->id)->get();

        foreach ($this->chat_rooms as $key => $room) {
            // $unseen_msgs = ;
            $this->chat_rooms[$key]->unseen_msg_count = $room->chats()->whereNot('sender_id', Auth::id())->where('is_seen', 0)->where('deleted_from_receiver', 0)->count();
            $this->ada_chat_baru += $this->chat_rooms[$key]->unseen_msg_count;
        }

        // $this->user = $this->photo;
        // $this->initProfileData();
        $this->mengajak_berteman = count($friend_request_ids) > 0;
        // $this->ada_chat_baru = "";


        if (!$is_search_name && !$is_search_gender && !$is_search_location) {
            $this->requestFriends = Friend::where('friend_id', Auth::id())->where('is_accepted', 0)->get();

            $friends = $this->user->acceptedFriends()->where(function ($query) {
                $query->where('name', 'like', "%" . $this->search . "%")->orWhere('username', 'like', "%" . $this->search . "%");
            })->get();
            $users = User::where(function ($query) {
                $query->where('name', 'like', "%" . $this->search . "%")->orWhere('username', 'like', "%" . $this->search . "%");
            })->whereNotIn('id', $this->blacklisted_ids)->get();
            $friend_requests = $this->user->friendsRequested()->where(function ($query) {
                $query->where('name', 'like', "%" . $this->search . "%")->orWhere('username', 'like', "%" . $this->search . "%");
            })->get();
            $pending_requests = $this->user->requestedFriends()->where(function ($query) {
                $query->where('name', 'like', "%" . $this->search . "%")->orWhere('username', 'like', "%" . $this->search . "%");
            })->get();
            $this->pending_requests = $pending_requests;
            $this->friend_requests = $friend_requests;
            $this->users = $users;
            $this->friends = $friends;
        }

        return view('livewire.dashboard')->extends('layouts.app')->section('content');
    }

    public function addFriend(User $user)
    {
        // dd("Masuk");
        // Get data that user if he already add me as friend
        $potentialFriend = $this->user->friendsRequested->where('id', $user->id)->first();
        $accepted        = 0;
        $msg             = 'Friend request sent successfully!';

        // if that user already send friend request
        if ($potentialFriend) {
            $potentialFriend->friends->where('id', $this->user->id)->first()->friend->is_accepted = 1;
            $potentialFriend->friends->where('id', $this->user->id)->first()->friend->save();

            $accepted = 1;
            $msg      = "$user->name has become your friend!";
        }

        // Get data logged in user if he already sent friend requrst
        $requestedFriend = $this->user->requestedFriends()->where('friend_id', $user->id)->first();

        // if logged in user doesn't send friend request before
        if (!isset($requestedFriend)) {
            $this->user->friends()->attach($user->id, ['is_asked' => 1, 'is_accepted' => $accepted]);
        }

        // $this->alert('success', "Success!", [
        //     'toast'    => false,
        //     'text'     => $msg,
        //     'position' => 'center',
        // ]);

    }

    public function cancelAddFriend(User $user)
    {
        // dd($this->user->friends);
        $this->user->friends()->detach($user->id);
        // $this->getProfile($user);
        $this->is_request_friend = false;
        $this->alert('success', 'Sucees!', [
            'text' => "You are canceled friend request with $user->name!"
        ]);
    }

    public function unFriend(User $user)
    {
        Friend::whereUserId(Auth::id())->whereFriendId($user->id)->first()->delete();
        Friend::whereFriendId(Auth::id())->whereUserId($user->id)->first()->delete();
        // $this->alert('success', 'Sucees!', [
        //     'text' => "You are no longer friends with $user->name!"
        // ]);
    }

    public function getProfile($user_id)
    {
        $this->reset(['isFriend', 'is_friend_blocked', 'is_request_friend', 'is_friend_request']);
        $friend = $this->user->acceptedFriends()->where('friend_id', $user_id)->first();
        $request_friend = $this->user->requestedFriends()->where('friend_id', $user_id)->first();
        $friend_request = $this->user->friendsRequested()->where('friend_id', $user_id)->first();

        // Get detail profile data of other user for right side menu
        if ($friend) {
            $this->isFriend = true;
            if ($friend->acceptedFriend->is_blocked == 1) {
                $this->is_friend_blocked = true;
            } else {
                $this->is_friend_blocked = false;
            }
        } elseif ($request_friend) {
            $this->isFriend = false;
            $this->is_request_friend = true;
            $this->is_friend_blocked = false;
        } elseif ($friend_request) {
            $this->isFriend = false;
            $this->is_friend_request = true;
            $this->is_friend_blocked = false;
        } else {
            $this->isFriend = false;
            $this->is_friend_blocked = false;
        }
        // dd($this->isFriend);
        $this->user_yg_dipilih = User::whereId($user_id)->first();
    }

    public function blockFriend(User $user)
    {
        $this->user->acceptedFriends->where('id', $user->id)->first()->acceptedFriend->is_blocked = 1;
        $this->user->acceptedFriends->where('id', $user->id)->first()->acceptedFriend->save();
        $this->is_friend_blocked = true;
        $this->alert('success', 'Sucees!', [
            'text' => "You are now block this friend!"
        ]);
    }

    public function unblockFriend(User $user)
    {
        $this->user->acceptedFriends->where('id', $user->id)->first()->acceptedFriend->is_blocked = 0;
        $this->user->acceptedFriends->where('id', $user->id)->first()->acceptedFriend->save();
        $this->is_friend_blocked = false;
        $this->alert('success', 'Sucees!', [
            'text' => "You are now unblock this friend!"
        ]);
    }

    public function openRoom(ChatRoom $room, $receiver_id)
    {
        // dd($room);
        $this->room = $room;
        $this->user_yg_dipilih = User::find($receiver_id);
        // $receiver = $room->participants->where('user_id', '!=', Auth::id())->first()->user;

        if (!$this->is_friend_blocked) {
            $room->chats()->whereNot('sender_id', Auth::id())->where('deleted_from_receiver', 0)->update([
                'is_seen' => 1,
            ]);
        }
        // $this->changeActiveChat($receiver_id);
        $this->getProfile($receiver_id);
        $this->emit('openChatModal');
        $this->refreshChat();
    }

    public function submitChat()
    {
        // dd("anjay");

        // dd($this->chat_input);

        $this->room = ChatRoom::hasUser($this->user->id)->hasUser($this->user_yg_dipilih->id)->first();

        if (!$this->room) {
            $this->room = ChatRoom::create([
                'name' => "{$this->user->name} to {$this->user_yg_dipilih->name}"
            ]);
            $this->room->users()->sync([$this->user->id, $this->user_yg_dipilih->id]);
        }

        if ($this->chat_input && $this->chat_input != '' && $this->chat_input != null) {
            if ($this->user_yg_dipilih->acceptedFriends->where('id', $this->user->id)->first()->acceptedFriend->is_blocked == 1) {
                $this->room->chats()->create([
                    'sender_id'             => $this->user->id,
                    'content'               => $this->chat_input,
                    'deleted_from_receiver'    => 1,
                ]);
            } else {
                $this->room->chats()->create([
                    'sender_id' => $this->user->id,
                    'content'   => $this->chat_input,
                ]);
            }
            $this->reset('chat_input');
        }

        $this->refreshChat();
    }

    public function updatedMediaChat($value)
    {
        $this->room = ChatRoom::hasUser($this->user->id)->hasUser($this->user_yg_dipilih->id)->first();
        $result = '';

        if (!$this->room) {
            $this->room = ChatRoom::create([
                'name' => "{$this->user->name} to {$this->user_yg_dipilih->name}"
            ]);
            $this->room->users()->sync([$this->user->id, $this->user_yg_dipilih->id]);
        }

        if ($value && $value != '' && !is_string($value)) {

            $extension = $value->getClientOriginalExtension();
            if ($extension == 'png' || $extension == 'jpg') {
                $result = $this->_uploadFile($value, 'images/chat');
            } elseif ($extension == 'mp4') {
                $result = $this->_uploadFile($value, 'video/chat');
            } else {
                $result = $this->_uploadFile($value, 'document/chat');
            }

            if ($result['path']) {
                if ($this->user_yg_dipilih->acceptedFriends->where('id', $this->user->id)->first()->acceptedFriend->is_blocked == 1) {
                    $this->room->chats()->create([
                        'sender_id'             => $this->user->id,
                        'is_file'               => 1,
                        'mime_type'               => $result['extension'],
                        'file_size'               => $result['file_size'],
                        'content'               => $result['filename'],
                        'deleted_from_receiver'    => 1,
                    ]);
                } else {
                    $this->room->chats()->create([
                        'sender_id'             => $this->user->id,
                        'is_file'               => 1,
                        'mime_type'               => $result['extension'],
                        'file_size'               => $result['file_size'],
                        'content'               => $result['filename'],
                    ]);
                }
            }
        }
        // dd($value);
    }

    public function deleteChat($chat_id)
    {
        $chat = Chat::find($chat_id);

        // dd($chat->id);
        if ($chat->sender_id == $this->user->id) {
            $chat->deleted_from_sender = 1;
        } else {
            $chat->deleted_from_receiver = 1;
        }
        $chat->save();

        $this->alert('success', 'Sucees!', [
            'text' => "The message has been successfully deleted!"
        ]);
    }

    public function deleteAllChat()
    {
        if ($this->room) {
            foreach ($this->room->chats as $chat) {
                if ($chat->sender_id == $this->user->id) {
                    $chat->deleted_from_sender = 1;
                } else {
                    $chat->deleted_from_receiver = 1;
                }
                $chat->save();
            }
        }

        $this->alert('success', 'Sucees!', [
            'text' => "All message history has been deleted!"
        ]);
    }

    public function unsendChat(Chat $chat)
    {
        if ($chat->sender_id == $this->user->id) {
            $chat->deleted_for_receiver = 1;
        }
        $chat->save();
        $this->alert('success', 'Sucees!', [
            'text' => "The message has been successfully unsend!"
        ]);
    }

    public function openRoomIfExist($friend_id)
    {

        $this->reset('room');
        $room = User::find(Auth::id())->rooms()->whereHas('participants', function ($q) use ($friend_id) {
            $q->where('user_id', $friend_id);
        })->first();

        if ($room) {
            $this->openRoom($room, $friend_id);
        } else {

            $this->getProfile($friend_id);
        }
    }

    public function refreshChat()
    {
        $this->reset(['chats']);

        if ($this->room) {
            $this->chats = $this->room->chats;
        }
    }

    public function updatedSearch($value)
    {
        if (!$value || $value == '' || $value == null) {
            $value = null;
        }
        if ($this->search_gender && $this->search_gender != '' && $this->search_gender != null) {
            if ($this->search_location && $this->search_location != '' && $this->search_location != null) {
                $this->search($value, $this->search_gender, $this->search_location);
            } else {
                $this->search($value, $this->search_gender);
            }
        } else {
            if ($this->search_location && $this->search_location != '' && $this->search_location != null) {
                $this->search($value, null, $this->search_location);
            } else {
                $this->search($value);
            }
        }
    }

    public function updatedSearchGender($value)
    {
        if (!$value || $value == '' || $value == null) {
            $value = null;
        }
        if ($this->search && $this->search != '' && $this->search != null) {
            if ($this->search_location && $this->search_location != '' && $this->search_location != null) {
                $this->search($this->search, $value, $this->search_location);
            } else {
                $this->search($this->search, $value);
            }
        } else {
            if ($this->search_location && $this->search_location != '' && $this->search_location != null) {
                $this->search(null, $value, $this->search_location);
            } else {
                $this->search(null, $value);
            }
        }
    }

    public function updatedSearchLocation($value)
    {
        if (!$value || $value == '' || $value == null) {
            $value = null;
        }
        if ($this->search && $this->search != '' && $this->search != null) {
            if ($this->search_gender && $this->search_gender != '' && $this->search_gender != null) {
                $this->search($this->search, $this->search_gender, $value);
            } else {
                $this->search($this->search, null, $value);
            }
        } else {
            if ($this->search_gender && $this->search_gender != '' && $this->search_gender != null) {
                $this->search(null, $this->search_gender, $value);
            } else {
                $this->search(null, null, $value);
            }
        }
    }

    public function search($value = null, $gender = null, $location = null)
    {
        // dd($gender);
        $search = ($value && $value != '' && $value != null) ? true : false;
        $search1 = ($gender && $gender != '' && $gender != null) ? true : false;
        $search2 = ($location && $location != '' && $location != null) ? true : false;
        // dd($search, $search1, $search2);

        if ($search && $search1 && $search2) {
            $friends = $this->user->acceptedFriends()->where(function ($query) use ($value) {
                $query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
            })->where('gender', $gender)->where('location', $location)->get();
            $allUser = User::where(function ($query) use ($value) {
                $query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
            })->where('gender', $gender)->where('location', $location)->whereNotIn('id', $this->blacklisted_ids)->get();
            $friend_requests = $this->user->friendsRequested()->where(function ($query) use ($value) {
                $query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
            })->where('gender', $gender)->where('location', $location)->get();
            $pending_requests = $this->user->requestedFriends()->where(function ($query) use ($value) {
                $query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
            })->where('gender', $gender)->where('location', $location)->get();
        } elseif ($search && $search1 && !$search2) {
            $friends = $this->user->acceptedFriends()->where(function ($query) use ($value) {
                $query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
            })->where('gender', $gender)->get();
            $allUser = User::where(function ($query) use ($value) {
                $query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
            })->where('gender', $gender)->whereNotIn('id', $this->blacklisted_ids)->get();
            $friend_requests = $this->user->friendsRequested()->where(function ($query) use ($value) {
                $query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
            })->where('gender', $gender)->get();
            $pending_requests = $this->user->requestedFriends()->where(function ($query) use ($value) {
                $query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
            })->where('gender', $gender)->get();
        } elseif ($search && !$search1 && $search2) {
            $friends = $this->user->acceptedFriends()->where(function ($query) use ($value) {
                $query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
            })->where('location', $location)->get();
            $allUser = User::where(function ($query) use ($value) {
                $query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
            })->where('location', $location)->whereNotIn('id', $this->blacklisted_ids)->get();
            $friend_requests = $this->user->friendsRequested()->where(function ($query) use ($value) {
                $query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
            })->where('location', $location)->get();
            $pending_requests = $this->user->requestedFriends()->where(function ($query) use ($value) {
                $query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
            })->where('location', $location)->get();
        } elseif (!$search && $search1 && $search2) {
            $friends = $this->user->acceptedFriends()->where('gender', $gender)->where('location', $location)->get();
            $allUser = User::where('gender', $gender)->where('location', $location)->whereNotIn('id', $this->blacklisted_ids)->get();
            $friend_requests = $this->user->friendsRequested()->where('gender', $gender)->where('location', $location)->get();
            $pending_requests = $this->user->requestedFriends()->where('gender', $gender)->where('location', $location)->get();
        } elseif (!$search && $search1 && !$search2) {
            $friends = $this->user->acceptedFriends()->where('gender', $gender)->get();
            $allUser = User::where('gender', $gender)->whereNotIn('id', $this->blacklisted_ids)->get();
            $friend_requests = $this->user->friendsRequested()->where('gender', $gender)->get();
            $pending_requests = $this->user->requestedFriends()->where('gender', $gender)->get();
        } elseif (!$search && !$search1 && $search2) {
            $friends = $this->user->acceptedFriends()->where('location', $location)->get();
            $allUser = User::where('location', $location)->whereNotIn('id', $this->blacklisted_ids)->get();
            $friend_requests = $this->user->friendsRequested()->where('location', $location)->get();
            $pending_requests = $this->user->requestedFriends()->where('location', $location)->get();
        } else {
            $friends = $this->user->acceptedFriends()->where(function ($query) use ($value) {
                $query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
            })->get();
            $allUser = User::where(function ($query) use ($value) {
                $query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
            })->whereNotIn('id', $this->blacklisted_ids)->get();
            $friend_requests = $this->user->friendsRequested()->where(function ($query) use ($value) {
                $query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
            })->get();
            $pending_requests = $this->user->requestedFriends()->where(function ($query) use ($value) {
                $query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
            })->get();
        }
        $this->friends = $friends;
        $this->pending_requests = $pending_requests;
        $this->friend_requests = $friend_requests;
        $this->users = $allUser;
        // dd($this->others = $user_yg_dipilih);

    }

    public function clearSearchUser()
    {
        $this->reset('search');
    }
}
