<?php

namespace App\Http\Livewire\User;

use Livewire\Component;

class Edit extends Component
{
    public $name;
    public $password;
    public $phone;

    // public function mount($id)
    // {
    //     $user = User::find($id);

    //     if ($user) {
    //         $this->name = $name;
    //         $this->password = $password;
    //         $this->phone = $phone;
    //     }
    // }

    public function render()
    {
        return view('livewire.user.edit');
    }

    // public function update(Request $request, Post $post)
    // {
    //     $request->validate([
    //         'desc' => 'required',
    //     ]);

    //     $post->desc = $request->desc;

    //     $post->save();

    //     return back();
    // }

    // public function destroy(Post $post)
    // {
    //     $post->delete();

    //     return back();
    // }
}
