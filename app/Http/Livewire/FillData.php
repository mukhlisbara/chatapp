<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Illuminate\Validation\Rule;
use Livewire\Component;

class FillData extends Component
{
    use LivewireAlert;

    public $gender, $location, $phone, $username, $password, $password_confirmation, $is_new, $user;

    public function rules()
    {
        return [
            'username'     => ['regex:/^\S*$/u', Rule::unique('users')->ignore(Auth::id()), 'nullable'],
            'password'     => ['confirmed', 'nullable'],
            'phone'        => ['numeric', 'nullable'],
        ];
    }

    public function mount($new = null)
    {
        if (Auth::user()->phone != null) {
            return redirect()->route('dashboard');
        }
        $this->user = Auth::user();
        $this->is_new = $new;

        // Init data for input
        $this->gender = 'male';
        $this->location = 'surabaya';
    }

    public function render()
    {
        return view('livewire.fill-data')->extends('layouts.app')->section('content');
    }

    public function checkInput()
    {
        $this->validate();
    }

    public function fillData()
    {
        // dd(Auth::id());
        $this->validate([
            'gender'     => 'required',
            'location'     => 'required',
            'phone'     => ['required', 'numeric'],
            'username'     => ['required', 'regex:/^\S*$/u', Rule::unique('users')->ignore(Auth::id())],
            'password'     => ['required', 'confirmed'],
        ]);

        // Get user data from database
        $user = User::find(Auth::id());

        // FIll the data and edit the data in database
        $user->fill([
            'username'    => $this->username,
            'phone'       => $this->phone,
            'gender'      => $this->gender,
            'location'    => $this->location,
            'password'    => $this->password,
        ]);

        $user->save();

        $this->flash('success', 'Successfully submitted form', [
            'toast'        => false,
            'text'         => 'Thank you for completing the registration, please enjoy our beautiful feature !!',
            'position'     => 'center',
        ], '/dashboard');

        // dd('test');

        // return redirect()->route('home');
    }

    public function welcomeMessage()
    {
        $this->confirm('Successfully Registed', [
            'icon'                    => 'info',
            'toast'                    => false,
            'html'                    => 'Thank you for your registration, this is your default credential <br> <p class="m-0"><b>Email : ' . Auth::user()->email . '</b></p><p class="m-0"><b>Username : ' . Auth::user()->username . '</b></p><p><b>Password : password</b></p> If you log out before you have finished providing your personal data, use the default credential to log in again !!',
            'position'                 => 'center',
            'showConfirmButton'        => true,
            'confirmButtonText'     => 'Yes, Noted It!!',
            'confirmButtonColor'    => '#3085d6',
        ]);
    }
}
