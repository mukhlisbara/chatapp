<div>
    <div id="app">
        <main class="py-4">
            <div class="container mt-5">
                <div class="row justify-content-center">
                    <div class="col-md-6 d-flex justify-content-center align-items-center">
                        <div class="w-100 my-5 d-flex justify-content-center align-items-center">
                            <img src="{{ asset('images/logo.png') }}" alt="logo" width="200"
                                class="shadow-light mx-auto d-block" style="height: 20rem; width: auto;">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card card-primary mx-auto" style="max-width: 500px">
                            <div class="card-header">{{ __('Register') }}

                            </div>
                            <div class="card-body">
                                <form method="POST" action="{{ route('register') }}"
                                    wire:submit.prevent="registerUser">
                                    @csrf

                                    <div class="form-group">
                                        <label for="name" class="col-md-4 col-form-label">Full Name<span
                                                class="text-danger">*</span></label>

                                        <div class="col-md-12" style="max-width: 500px">
                                            <input id="name" type="text"
                                                class="form-control @error('name') is-invalid @enderror" name="name"
                                                value="{{ old('name') }}" required autocomplete="name" autofocus
                                                wire:model.defer="name" placeholder="Example">

                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="email"
                                            class="col-md-4 col-form-label">{{ __('Email Address') }}<span
                                                class="text-danger">*</span></label>

                                        <div class="col-md-12">
                                            <input id="email" type="email"
                                                class="form-control @error('email') is-invalid @enderror" name="email"
                                                value="{{ old('email') }}" required autocomplete="email"
                                                wire:model.defer="email" placeholder="name@example.com">

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="gender" class="col-md-4 col-form-label">Gender<span
                                                class="text-danger">*</span></label>
                                        <div class="col-md-12">
                                            <select class="form-select @error('gender') is-invalid @enderror"
                                                name="gender" id="gender" value="{{ old('gender') }}" required
                                                autocomplete="gender" wire:model.defer="gender">
                                                <option value="male" selected>Male</option>
                                                <option value="female">Female</option>
                                            </select>
                                            @error('gender')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="location" class="col-md-4 col-form-label">Location<span
                                                class="text-danger">*</span></label>
                                        <div class="col-md-12">
                                            <select class="form-select @error('location') is-invalid @enderror"
                                                name="location" id="location" value="{{ old('location') }}" required
                                                autocomplete="location" wire:model.defer="location">
                                                <option value="surabaya" selected>Surabaya</option>
                                                <option value="jakarta">Jakarta</option>
                                            </select>
                                            @error('location')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="name" class="col-md-4 col-form-label">Phone Number<span
                                                class="text-danger">*</span></label>

                                        <div class="col-md-12" style="max-width: 500px">
                                            <input id="phone" type="text"
                                                class="form-control @error('phone') is-invalid @enderror"
                                                value="{{ old('phone') }}" required autocomplete="phone" autofocus
                                                wire:model.defer="phone" placeholder="Example"
                                                wire:change='checkInput'>

                                            @error('phone')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="username" class="col-md-4 col-form-label">{{ __('Username') }}<span
                                                class="text-danger">*</span></label>

                                        <div class="col-md-12">
                                            <input id="username" type="username"
                                                class="form-control @error('username') is-invalid @enderror"
                                                name="username" value="{{ old('username') }}" required
                                                autocomplete="username" wire:model.defer="username"
                                                placeholder="Username">

                                            @error('username')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="password"
                                            class="col-md-4 col-form-label">{{ __('Password') }}<span
                                                class="text-danger">*</span></label>

                                        <div class="col-md-12">
                                            <input id="password" type="password"
                                                class="form-control @error('password') is-invalid @enderror"
                                                name="password" required autocomplete="new-password"
                                                wire:model.defer="password" placeholder="*************">

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="password-confirm"
                                            class="col-md-4 col-form-label">{{ __('Confirm Password') }}<span
                                                class="text-danger">*</span></label>

                                        <div class="col-md-12">
                                            <input id="password-confirm" type="password" class="form-control"
                                                name="password_confirmation" required autocomplete="new-password"
                                                wire:model.defer="password_confirmation" placeholder="*************">
                                        </div>
                                    </div>

                                    <div class="form-group mt-3">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-primary w-100">
                                                {{ __('Register') }}
                                            </button>
                                        </div>

                                        <div class="mt-3 text-center">
                                            Have account ? <a href="{{ route('login') }}" class="tex-primary">SignIn
                                                Now</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    {{-- PWA --}}
    <script src="{{ asset('/sw.js') }}"></script>
    <script>
        if (!navigator.serviceWorker.controller) {
            navigator.serviceWorker.register("/sw.js").then(function(reg) {
                console.log("Service worker has been registered for scope: " + reg.scope);
            });
        }
    </script>
</div>
