<div>
    <div id="app">
        <main class="py-4">
            <div class="container mt-5">
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="login-brand mb-3">
                            <img src="{{ asset('images/logo.png') }}" alt="logo" width="250"
                                class="shadow-light mx-auto d-block" style="max-width: 500px">
                        </div>
                        <div class="card card-primary mx-auto" style="max-width: 500px ">
                            <div class="card-header">{{ __('Login') }}</div>

                            <div class="card-body">
                                <form method="POST" action="{{ route('login') }}" class="needs-validation"
                                    novalidate="" wire:submit.prevent="loginUser">
                                    @csrf
                                    <div class="form-group">
                                        <label for="email" class="col-md-6 col-form-label">
                                            {{ __('Email Address or Username') }}
                                        </label>
                                        <div class="col-md-12">
                                            <input id="email" type="email"
                                                class="form-control @error('email') is-invalid @enderror" name="email"
                                                value="{{ old('email') }}" required autocomplete="email" autofocus
                                                placeholder="name@example.com / Username" wire:model.defer="email">
                                            @error('email')
                                                <div class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="password"
                                            class="col-md-4 col-form-label">{{ __('Password') }}</label>
                                        <div class="col-md-12">
                                            <input id="password" type="password"
                                                class="form-control @error('password') is-invalid @enderror"
                                                name="password" required autocomplete="current-password"
                                                placeholder="************" wire:model.defer="password">
                                            @error('password')
                                                <div class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group mt-2">
                                        <div class="col-md-6 ">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="remember"
                                                    id="remember" {{ old('remember') ? 'checked' : '' }}
                                                    wire:model.defer="remember">

                                                <label class="form-check-label" for="remember">
                                                    {{ __('Remember Me') }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group mt-3">
                                        <div class="col-md-12 ">
                                            <button type="submit" class="btn btn-primary w-100">
                                                {{ __('Login') }}
                                            </button>
                                        </div>

                                        <div class="col-md-12 mt-3">
                                            <div class="text-center mb-2">
                                                Or Sign In With
                                            </div>
                                            <a class="btn btn-danger w-100" href="{{ route('google.login') }}">
                                                <i class="fab fa-google-plus mr-2"></i>Login With Google
                                            </a>
                                        </div>
                                        <div class="mt-5 text-center">
                                            No have account ? <a href="{{ route('register') }}"
                                                class="tex-primary">SignUp Now</a>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    {{-- PWA --}}
    <script src="{{ asset('/sw.js') }}"></script>
    <script>
        if (!navigator.serviceWorker.controller) {
            navigator.serviceWorker.register("/sw.js").then(function(reg) {
                console.log("Service worker has been registered for scope: " + reg.scope);
            });
        }
    </script>
</div>
