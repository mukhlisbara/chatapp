<div>
    <style>
        body .modal-video .modal-dialog,
        body .modal-foto .modal-dialog,
        body .modal-editProfile .modal-dialog,
            {
            /* Width */
            max-width: 100%;
            width: auto !important;
            /* display: inline-block; */
        }

        .modal-video,
        .modal-foto,
        .modal-editProfile,
            {
            z-index: -1;
            display: flex !important;
            justify-content: center;
            align-items: center;
        }

        .modal-open .modal-video,
        .modal-open .modal-foto,
        .modal-open .modal-editProfile,
            {
            z-index: 1060;
        }

        @media (max-width: 767.98px) {
            .modal-fullscreen-md-down .modal-content {
                height: initial !important;
                display: block;
                overflow: auto;
            }
        }
    </style>
    {{-- MODAL EDIT --}}

    {{-- Start Modal Foto --}}
    <div class="modal fade modal-foto" wire:ignore id="imageModal" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-fullscreen-sm-down modal-lg modal-dialog-centered">
            <div class="modal-content overflow-hidden">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body p-0 d-flex align-items-center justify-content-center" wire:ignore>
                    <img src="" alt="" class=""
                        style="object-fit: contain ; min-height:50vh ; max-height: 300px; width:auto ; max-width:70vw">
                </div>
            </div>
        </div>
    </div>
    {{-- End Modal Foto --}}

    {{-- Start Modal Video --}}
    <div class="modal fade modal-video " wire:ignore.self id="videoModal" tabindex="-1"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-fullscreen-sm-down modal-lg modal-dialog-centered ">
            <div class="modal-content overflow-hidden">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body p-0 d-flex align-items-center justify-content-center">
                    <video class=""
                        style="object-fit: contain ; min-height:50vh ; max-height: 300px; width:auto ; max-width:70vw"
                        controls>
                        <source src="">
                        Your browser does not support HTML video.
                    </video>
                </div>
            </div>
        </div>
    </div>
    {{-- End Modal Video --}}

    {{-- Start Edit Profile Modal --}}
    <div class="modal fade modal-editProfile" wire:ignore.self id="editProfile" tabindex="-1" role="dialog"
        aria-labelledby="editProfile" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-size-16" id="editProfile">
                        Edit Profile</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <form>
                    <div class="modal-body p-4 custom-scrollbar" style="overflow-y:scroll; height:80vh">
                        <div class="mb-3">
                            <div class="form" @error('name') is-invalid @enderror>
                                <label for="editProfilename-input" class="form-label">Full Name</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror"
                                    id="editProfilename-input" placeholder="Enter FullName" wire:model="name">
                            </div>
                            @error('name')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <div class="form" @error('username') is-invalid @enderror>
                                <label for="editProfileusername-input" class="form-label">Username</label>
                                <input type="text" class="form-control @error('username') is-invalid @enderror"
                                    id="editProfileusername-input" placeholder="Enter Username" wire:model="username">
                            </div>
                            @error('username')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <div class="form" @error('email') is-invalid @enderror>
                                <label for="editProfileemail-input" class="form-label">Email</label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror"
                                    id="editProfileemail-input" placeholder="Enter Email" wire:model="email">
                            </div>
                            @error('email')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mb-3" wire:key>
                            <label for="editProfilephoto-input" class="form-label">Photo Profile</label>
                            <input type="file" class="form-control" id="editProfilephoto-input"
                                placeholder="Enter Photo" wire:model="photo">
                        </div>
                        <div class="mb-3">
                            <div class="form" @error('phone') is-invalid @enderror>
                                <label for="editProfilephone-input" class="form-label">Phone</label>
                                <input type="text" class="form-control @error('phone') is-invalid @enderror"
                                    id="editProfilephone-input" placeholder="Enter Phone" wire:model="phone">
                            </div>
                            @error('phone')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <div class="form @error('gender') is-invalid @enderror">
                                <label for="editProfilegender-input" class="form-label">Gender</label>
                                {{-- <input type="text" class="form-control @error('gender') is-invalid @enderror"
                                id="editProfilegender-input" placeholder="Enter Gender" wire:model="gender"> --}}
                                <select class="form-control @error('gender') is-invalid @enderror"
                                    id="editProfilegender-select" wire:model="gender" required>
                                    <option value="male" selected>Male</option>
                                    <option value="female">Female</option>
                                </select>
                            </div>
                            @error('gender')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <div class="form @error('location') is-invalid @enderror">
                                <label for="editProfilelocation-input" class="form-label">Location</label>
                                {{-- <input type="text" class="form-control @error('location') is-invalid @enderror"
                                    id="editProfilelocation-input" placeholder="Enter Location"
                                    wire:model="location"> --}}
                                <select class="form-control @error('location') is-invalid @enderror"
                                    id="editProfilelocation-select" wire:model="location" required>
                                    <option value="Surabaya" selected>Surabaya</option>
                                    <option value="Jakarta">Jakarta</option>
                                </select>
                                @error('location')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="form @error('bio') is-invalid @enderror">
                                <label for="editProfile-invitemessage-input" class="form-label">Bio</label>
                                <textarea class="form-control @error('bio') is-invalid @enderror" id="editProfile-invitemessage-input"
                                    rows="3" maxlength="255" style="max-height: 50px" placeholder="Enter Bio" wire:model="bio"></textarea>
                                @error('bio')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-0">
                            <a href="javascript:void(0)" class="fs-6" data-bs-toggle="modal"
                                data-bs-target="#changePasswordModal">Change password?</a>
                        </div>


                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-bs-dismiss="modal">Cancel</button>
                        <button type="button" wire:click.prevent="checkEditedEmail" class="btn btn-warning"
                            data-bs-dismiss="modal">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- End Edit Profile Modal --}}
    {{-- Start change password modal --}}
    <div class="modal fade modal-changePassword" wire:ignore.self id="changePasswordModal" tabindex="-1"
        role="dialog" aria-labelledby="changePasswordModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content overflow-hidden ">
                <div class="modal-header">
                    <h5 class="modal-title font-size-16" id="changePasswordModal">
                        Change Password</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <form>
                    <div class="modal-body p-4">
                        <div class="input-group has-validation mb-3">
                            <div class="w-100 @error('old_password') is-invalid @enderror">
                                <label for="oldPassword">Old Password <span class="text-danger">*</span></label>
                                <input type="password"
                                    class="form-control @error('old_password') is-invalid @enderror" id="oldPassword"
                                    placeholder="********" wire:model.defer="old_password">
                            </div>
                            @error('old_password')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="input-group has-validation mb-3">
                            <div class="w-100 @error('new_password') is-invalid @enderror">
                                <label for="newPassword">New Password <span class="text-danger">*</span></label>
                                <input type="password"
                                    class="form-control @error('new_password') is-invalid @enderror" id="newPassword"
                                    placeholder="********" wire:model.defer="new_password">
                            </div>
                            @error('new_password')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="input-group has-validation mb-3">
                            <div class="w-100 @error('new_password_confirmation') is-invalid @enderror">
                                <label for="confirmNewPassword">New Password Confirmation<span
                                        class="text-danger">*</span></label>
                                <input type="password"
                                    class="form-control @error('new_password_confirmation') is-invalid @enderror"
                                    id="confirmNewPassword" placeholder="********"
                                    wire:model.defer="new_password_confirmation">
                            </div>
                            @error('new_password_confirmation')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-bs-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-warning" wire:click.prevent="changePassword"
                            data-bs-dismiss="modal">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- End Change Password Modal --}}
    {{-- END MODAL --}}

    <div class="layout-wrapper d-lg-flex">
        <!-- Start left sidebar-menu -->
        <div class="side-menu flex-lg-column me-lg-1 ms-lg-0">
            <!-- LOGO -->
            <div class="navbar-brand-box logo-sm mt-4">
                <img src="{{ asset('/images/logo.png') }}" alt="" height="30">
            </div>
            <!-- end navbar-brand-box -->

            <!-- Start side-menu nav -->
            <div class="flex-lg-column my-auto">
                <ul class="nav nav-pills side-menu-nav justify-content-center" role="tablist">

                    <li class="nav-item " data-bs-toggle="tooltip" data-bs-placement="top" title="Profile">
                        <a class="nav-link {{ $navbar == 'profile' ? ' active' : '' }}" id="pills-user-tab"
                            wire:click="gantiTab('profile')" data-bs-toggle="pill" href="#pills-user"
                            role="tab">
                            <i class="ri-user-2-line"></i>
                        </a>
                    </li>
                    <li class="nav-item " data-bs-toggle="tooltip" data-bs-placement="top" title="Chats">
                        <a class="nav-link {{ $navbar == 'chat' ? ' active' : '' }} btn position-relative"
                            id="pills-chat-tab" data-bs-toggle="pill" wire:click="gantiTab('chat')"
                            href="#pills-chat" role="tab">
                            <i class="ri-message-3-line"></i>
                            @if ($ada_chat_baru)
                                <span
                                    class="position-absolute top-0 start-100 translate-middle p-2 bg-danger border border-light rounded-circle">
                                </span>
                            @endif
                        </a>
                    </li>
                    <li class="nav-item" data-bs-toggle="tooltip" data-bs-placement="top" title="Contacts">
                        <a class="nav-link {{ $navbar == 'contact' ? ' active' : '' }} btn position-relative"
                            id="pills-contacts-tab" data-bs-toggle="pill" wire:click="gantiTab('contact')"
                            href="#pills-contacts" role="tab">
                            <i class="ri-contacts-line"></i>
                            @if ($mengajak_berteman)
                                <span
                                    class="position-absolute top-0 start-100 translate-middle p-2 bg-danger border border-light rounded-circle">
                                </span>
                            @endif

                        </a>
                    </li>
                    <li class="nav-item" data-bs-toggle="tooltip" data-bs-placement="top" title="All User">
                        <a class="nav-link {{ $navbar == 'user' ? ' active' : '' }} " id="pills-users-tab"
                            data-bs-toggle="pill" wire:click="gantiTab('user')" href="#pills-users" role="tab">
                            <i class="ri-group-line"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link light-dark-mode" href="#" data-bs-toggle="tooltip"
                            data-bs-trigger="hover" data-bs-placement="right" title="Dark / Light Mode">
                            <i class='ri-sun-line theme-mode-icon'></i>
                        </a>
                    </li>
                    <li class="nav-item dropdown profile-user-dropdown d-inline-block d-lg-none" wire:ignore>
                        <a class="nav-link dropdown-toggle" href="javascript::void(0)" role="button"
                            data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{ $user->photo == 'user.png' ? asset("images/users/$user->photo") : asset("images/profile/$user->photo") }}"
                                style="object-fit: cover" alt="" class="profile-user rounded-circle">
                        </a>
                        <div class="dropdown-menu">

                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                                <i class="ri-logout-circle-r-line float-end text-muted"></i>
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- end side-menu nav -->

            <div class="flex-lg-column d-none d-lg-block">
                <ul class="nav side-menu-nav justify-content-center">
                    <li class="nav-item btn-group dropup profile-user-dropdown">
                        <a class="nav-link dropdown-toggle" wire:ignore.self href="javascript::void(0)"
                            role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{ $user->photo == 'user.png' ? asset("images/users/$user->photo") : asset("images/profile/$user->photo") }}"
                                style="object-fit: cover" alt="" class="profile-user rounded-circle">
                        </a>
                        <div class="dropdown-menu" wire:ignore.self>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                                <i class="ri-logout-circle-r-line float-end text-muted"></i>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- Side menu user -->
        </div>
        <!-- end left sidebar-menu -->

        <!-- start chat-leftsidebar -->
        <div class="chat-leftsidebar me-lg-1 ms-lg-0">

            <div class="tab-content">
                <!-- Start Profile tab-pane -->
                <div class="tab-pane {{ $tabpane == 'profile' ? 'fade show active' : '' }}" id="pills-user"
                    role="tabpanel" aria-labelledby="pills-user-tab">
                    <!-- Start profile content -->
                    <div>
                        <div class="px-4 pt-4">
                            <div class="user-chat-nav float-end">
                                <button type="button" class="btn btn-light btn-sm float-end" data-bs-toggle="modal"
                                    data-bs-target="#editProfile">
                                    <i class="ri-edit-fill me-1 ms-0 "></i> Edit
                                </button>
                            </div>
                            <h4 class="mb-0">My Profile</h4>
                        </div>

                        <div class="text-center p-4 border-bottom">
                            <div class="position-relative mb-4">
                                {{-- <a href="javascript::void(0)" data-bs-toggle="modal" data-bs-target="#imageModal"
                                    data-bs-image="{{ $user->photo == 'user.png' ? asset("images/users/$user->photo") : asset("images/profile/$user->photo") }}"
                                    class="h-100 w-100">
                                    <img src="{{ $user->photo == 'user.png' ? asset("images/users/$user->photo") : asset("images/profile/$user->photo") }}"
                                        class="rounded-circle avatar-lg img-thumbnail" style="object-fit: cover"
                                        alt="">
                                </a> --}}
                                <img src="{{ $user->photo == 'user.png' ? asset("images/users/$user->photo") : asset("images/profile/$user->photo") }}"
                                    class="rounded-circle avatar-lg img-thumbnail preview-img"
                                    style="object-fit: cover; cursor: pointer;" alt=""
                                    data-bs-target="#imageModal">
                                <span
                                    class="position-absolute translate-middle d-flex align-items-center justify-content-center rounded-circle bg-danger"
                                    style="cursor: pointer; width: 35px; height: 35px; left: 60%; top: 85%; color: white;"
                                    id="resetPhoto">
                                    <i class="fa-solid fa-trash fs-6"></i>
                                    <span class="visually-hidden">Reset Photo</span>
                                </span>
                            </div>
                            <h5 class="font-size-16 mb-1 text-truncate">{{ $user->name }} {!! $user->gender == 'male'
                                ? '<i class="fa-solid fa-mars fs-5 text-primary"></i>'
                                : '<i class="fa-solid fa-venus fs-5" style="color: #e0276a;"></i>' !!}
                            </h5>
                            <p class="text-muted text-truncate mb-1">
                                @ {{ $user->username }}</p>
                        </div>
                        <!-- End profile user -->

                        <!-- Start user-profile-desc -->
                        <div class="p-4 user-profile-desc custom-scrollbar" style="overflow-y: scroll">
                            <div class="text-muted">
                                <p class="mb-4">
                                    {{ $user->bio != null ? $user->bio : 'Bio Default' }}
                                </p>
                            </div>
                            <div id="tabprofile" class="accordion">
                                <div class="accordion-item card border mb-2">
                                    <div class="accordion-header" id="about2">
                                        {{-- <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#about" aria-expanded="true" aria-controls="about">
                                            <h5 class="font-size-14 m-0">
                                                <i
                                                    class="ri-user-2-line me-2 ms-0 ms-0 align-middle d-inline-block"></i>
                                                About
                                            </h5>
                                        </button> --}}
                                        <div class="accordion-button">
                                            <h5 class="font-size-14 m-0">
                                                <i
                                                    class="ri-user-2-line me-2 ms-0 ms-0 align-middle d-inline-block"></i>
                                                About
                                            </h5>
                                        </div>
                                    </div>
                                    <div id="about" class="accordion-collapse collapse show"
                                        aria-labelledby="about2" data-bs-parent="#tabprofile">
                                        <div class="accordion-body">
                                            <div>
                                                <p class="text-muted mb-1">Full Name</p>
                                                <h5 class="font-size-14">{{ $user->name }}</h5>
                                            </div>

                                            <div class="mt-4">
                                                <p class="text-muted mb-1">Email</p>
                                                <h5 class="font-size-14">{{ $user->email }}</h5>
                                            </div>

                                            <div class="mt-4">
                                                <p class="text-muted mb-1">Phone</p>
                                                <h5 class="font-size-14">
                                                    {{ $user->phone != null ? $user->phone : '-' }}
                                                </h5>
                                            </div>

                                            <div class="mt-4">
                                                <p class="text-muted mb-1">Location</p>
                                                <h5 class="font-size-14 mb-0">
                                                    {{ $user->location != null ? $user->location : '-' }}
                                                </h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End About card -->
                            </div>
                            <!-- end profile-user-accordion -->
                            <button type="button" class="btn btn-danger btn-sm float-start delete-account-btn">
                                <i class="ri-delete-bin-fill me-1 ms-0 "></i> Delete Account
                            </button>
                        </div>
                        <!-- end user-profile-desc -->
                    </div>
                    <!-- End profile content -->
                </div>
                <!-- End Profile tab-pane -->

                <!-- Start Chats tab-pane -->
                <div class="tab-pane {{ $tabpane == 'chat' ? 'fade show active' : '' }}" id="pills-chat"
                    role="tabpanel" aria-labelledby="pills-chat-tab">
                    <!-- Start chats content -->
                    <div>
                        <div class="px-4 pt-4 border-bottom">
                            <h4 class="mb-4">Chats</h4>
                        </div> <!-- .p-4 -->

                        <!-- Start chat-message-list -->
                        <div>
                            <div class="chat-message-list px-2" data-simplebar>

                                <ul class="list-unstyled chat-list chat-user-list custom-scrollbar"
                                    style="overflow-y:scroll ; max-height: 600px;">

                                    @forelse ($chat_rooms as $detail_room)
                                        @php
                                            $receiver = $detail_room->participants->where('user_id', '!=', Auth::id())->first()->user;
                                            $latest_chat = $detail_room->chats
                                                ->where('deleted_from_receiver', 0)
                                                ->where('deleted_from_sender', 0)
                                                ->last();
                                            // dd($latest_chat);
                                        @endphp
                                        {{-- @dd($detail_room->id) --}}

                                        <li
                                            class="{{ (!is_null($user_yg_dipilih) ? $user_yg_dipilih->id : 999999999999) === $receiver->id ? 'active' : '' }}">
                                            <a href="#"
                                                wire:click="openRoom({{ $detail_room->id }}, {{ $receiver->id }})">
                                                <div class="d-flex">

                                                    <div class="chat-user-img online align-self-center me-3 ms-0">
                                                        <img src="{{ $receiver->photo == 'user.png' ? asset("images/users/$receiver->photo") : asset("images/profile/$receiver->photo") }}"
                                                            class="rounded-circle avatar-xs" style="object-fit: cover"
                                                            alt="">
                                                    </div>

                                                    <div class="flex-grow-1 overflow-hidden">
                                                        <h5 class="text-truncate font-size-15 mb-1">
                                                            {{ $receiver->name }}
                                                        </h5>

                                                        <p class="chat-user-message text-truncate mb-0">
                                                            @php
                                                                if ($latest_chat->deleted_for_receiver) {
                                                                    $latest_chat->content = 'Pesan ini telah dihapus!';
                                                                }
                                                            @endphp
                                                            @if (isset($latest_chat))
                                                                @php
                                                                    $deleted_from_source = $latest_chat->sender_id == $user->id ? $latest_chat->deleted_from_sender : $latest_chat->deleted_from_receiver;
                                                                @endphp

                                                                @if (!$deleted_from_source)
                                                                    @if ($latest_chat->is_file)
                                                                        @if ($latest_chat->deleted_for_receiver)
                                                                            Pesan ini telah dihapus!
                                                                        @else
                                                                            @if ($latest_chat->mime_type == 'png' || $latest_chat->mime_type == 'jpg')
                                                                                <i
                                                                                    class="ri-image-fill align-middle me-1 ms-0"></i>
                                                                                Images
                                                                            @elseif ($latest_chat->mime_type == 'mp4')
                                                                                <i
                                                                                    class="ri-video-fill align-middle me-1 ms-0"></i>
                                                                                Video
                                                                            @else
                                                                                <i
                                                                                    class="ri-file-text-fill align-middle me-1 ms-0"></i>
                                                                                Document
                                                                            @endif
                                                                        @endif
                                                                    @else
                                                                        {{ $latest_chat->content }}
                                                                    @endif
                                                                @endif

                                                            @endif
                                                        </p>

                                                    </div>
                                                    <div
                                                        class="align-self-stretch my-1 flex-fill d-flex flex-column align-items-end">
                                                        <small
                                                            class="text-nowrap text-muted">{{ isset($latest_chat) ? $latest_chat->created_at->diffForHumans() : '' }}</small>
                                                        @if ($detail_room->unseen_msg_count > 0)
                                                            <span
                                                                class="badge bg-danger mt-1 rounded-circle fw-bold">{{ $detail_room->unseen_msg_count }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                    @empty
                                        <li class="d-flex align-items-center justify-content-center py-4">
                                            <h6 class="my-5 w-100 text-dark text-center">You doesn't have any chat</h6>
                                            <a href="javascript:void(0)" class="text-decoration-none text-center"
                                                wire:click="gantiTab('contact')">Start your first chat now?</a>
                                        </li>
                                    @endforelse

                                </ul>
                            </div>
                        </div>
                        <!-- End chat-message-list -->
                    </div>
                    <!-- Start chats content -->
                </div>
                <!-- End chats tab-pane -->

                <!-- Start Contacts tab-pane -->
                <div class="tab-pane {{ $tabpane == 'contact' ? 'fade show active' : '' }}" id="pills-contacts"
                    role="tabpanel" aria-labelledby="pills-contacts-tab">
                    <!-- Start Contact content -->
                    <div>
                        <div class="px-4 pt-4">
                            <h4 class="mb-4">Friends</h4>
                            {{-- Start search box --}}
                            <div class="search-box chat-search-box">
                                <div class="input-group bg-light  input-group-lg rounded-3">
                                    <div class="input-group-prepend">
                                        <button class="btn btn-link text-decoration-none text-muted pe-1 ps-3"
                                            type="button">
                                            <i class="ri-search-line search-icon font-size-18"></i>
                                        </button>
                                    </div>
                                    <input type="text" class="form-control bg-light"
                                        placeholder="Search Friends.." name="search" wire:model="search">
                                    @if ($search != '')
                                        <button class="btn close-search" type="button"
                                            wire:click="clearSearchUser()" style="color: grey;"><i
                                                class="fa-solid fa-xmark fs-6 "></i></button>
                                    @endif
                                </div>
                            </div>
                            <div class="d-flex justify-content-between align-items-center pb-2">
                                <select class="form-select form-select-sm me-2" wire:model="search_gender">
                                    <option value="" selected>Select
                                        Gender</option>
                                    <option value="male" {{ $search_gender == 'male' ? 'selected' : '' }}>Male
                                    </option>
                                    <option value="female" {{ $search_gender == 'female' ? 'selected' : '' }}>Female
                                    </option>
                                </select>
                                <select class="form-select form-select-sm ms-2" wire:model="search_location">
                                    <option value="" selected>
                                        Select Location</option>
                                    <option value="Surabaya" {{ $search_location == 'surabaya' ? 'selected' : '' }}>
                                        Surabaya</option>
                                    <option value="Jakarta" {{ $search_location == 'jakarta' ? 'selected' : '' }}>
                                        Jakarta</option>
                                </select>
                            </div>
                            <!-- End search-box -->
                            <!-- Start Add contact Modal -->
                            <div class="modal fade" id="addContact-exampleModal" tabindex="-1" role="dialog"
                                aria-labelledby="addContact-exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                    <div class="modal-content overflow-hidden">
                                        <div class="modal-header">
                                            <h5 class="modal-title font-size-16" id="addContact-exampleModalLabel">Add
                                                Contact</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close">
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Add contact Modal -->
                        </div>
                        <!-- end p-4 -->

                        <!-- Start contact lists -->
                        <div class="p-4 chat-message-list chat-group-list" data-simplebar>
                            <div>
                                <div class="p-3 fw-bold text-warning">
                                    Friend Request
                                </div>
                                <ul class="list-unstyled contact-list custom-scrollbar"
                                    style="overflow-y:scroll ; max-height: 600px;">
                                    @forelse ($friend_requests as $item)
                                        {{-- @dd($item) --}}
                                        <li>
                                            <div class="d-flex align-items-center">
                                                <div class="me-3 ms-0">
                                                    <img src="{{ $item->photo == 'user.png' ? asset("images/users/{$item->photo}") : asset("images/profile/{$item->photo}") }}"
                                                        class="rounded-circle avatar-xs" style="object-fit: cover"
                                                        alt="">
                                                </div>
                                                <div class="flex-grow-1">
                                                    <h5 class="font-size-14 m-0">{{ $item->name }}</h5>
                                                </div>
                                                <a href="#" wire:click="addFriend({{ $item->id }})">

                                                    <i class="ri-user-add-line float-end text-success"></i>
                                                </a>
                                            </div>
                                        </li>
                                    @empty
                                        <li class="d-flex align-items-center justify-content-center p-0">
                                            <h6 class="text-dark text-center my-4">No Friend Request Found</h6>
                                        </li>
                                    @endforelse
                                </ul>

                            </div>
                            <!-- end contact list A -->

                            <div class="mt-3">
                                <div class="p-3 fw-bold text-warning">
                                    Friend
                                </div>
                                <ul class="list-unstyled contact-list custom-scrollbar"
                                    style="overflow-y:scroll ; max-height: 600px;">

                                    @forelse ($friends as $item)
                                        <li>
                                            <div class="d-flex align-items-center">
                                                <div class="me-3 ms-0">
                                                    <img src="{{ $item->photo == 'user.png' ? asset("images/users/$item->photo") : asset("images/profile/$item->photo") }}"
                                                        class="rounded-circle avatar-xs" style="object-fit: cover"
                                                        alt="">
                                                </div>
                                                <a href="javascript:void(0)"
                                                    class="w-100 text-decoration-none text-dark d-flex align-items-center"
                                                    wire:click="openRoomIfExist({{ $item->id }})">
                                                    <div class="flex-grow-1">
                                                        <h5 class="font-size-14 m-0">{{ $item->name }}</h5>
                                                    </div>
                                                </a>
                                                <a class=" unfriend-btn" href="#"
                                                    data-id="{{ $item->id }}" data-name="{{ $item->name }}">
                                                    <i class="ri-user-unfollow-line float-end text-danger"></i>
                                                </a>
                                            </div>
                                        </li>
                                    @empty
                                        <li class="d-flex align-items-center justify-content-center p-0">
                                            <h6 class="text-dark text-center my-4">No have friend now</h6>
                                        </li>
                                    @endforelse
                                </ul>
                            </div>

                        </div>
                        <!-- end contact lists -->

                    </div>
                    <!-- Start Contact content -->
                </div>
                <!-- End contacts tab-pane -->

                <!-- Start User tab-pane -->
                <div class="tab-pane {{ $tabpane == 'user' ? 'fade show active' : '' }}" id="pills-users"
                    role="tabpanel" aria-labelledby="pills-users-tab">
                    <!-- Start User content -->
                    <div>
                        <div class="px-4 pt-4">
                            <h4 class="mb-4">All User</h4>
                            {{-- Start search box --}}
                            <div class="search-box chat-search-box">
                                <div class="input-group bg-light  input-group-lg rounded-3">
                                    <div class="input-group-prepend">
                                        <button class="btn btn-link text-decoration-none text-muted pe-1 ps-3"
                                            type="button">
                                            <i class="ri-search-line search-icon font-size-18"></i>
                                        </button>
                                    </div>
                                    <input type="text" class="form-control bg-light" placeholder="Search users.."
                                        name="search" wire:model="search">
                                    @if ($search != '')
                                        <button class="btn close-search" type="button"
                                            wire:click="clearSearchUser()" style="color: grey;"><i
                                                class="fa-solid fa-xmark fs-6"></i></button>
                                    @endif
                                </div>
                            </div>
                            <div class="d-flex justify-content-between align-items-center pb-2">
                                <select class="form-select form-select-sm me-2" wire:model="search_gender">
                                    <option value="" selected>Select
                                        Gender</option>
                                    <option value="male" {{ $search_gender == 'male' ? 'selected' : '' }}>Male
                                    </option>
                                    <option value="female" {{ $search_gender == 'female' ? 'selected' : '' }}>Female
                                    </option>
                                </select>
                                <select class="form-select form-select-sm ms-2" wire:model="search_location">
                                    <option value="" selected>
                                        Select Location</option>
                                    <option value="Surabaya" {{ $search_location == 'surabaya' ? 'selected' : '' }}>
                                        Surabaya</option>
                                    <option value="Jakarta" {{ $search_location == 'jakarta' ? 'selected' : '' }}>
                                        Jakarta</option>
                                </select>
                            </div>
                            <!-- End search-box -->
                        </div>
                        <!-- end p-4 -->

                        <!-- Start All User lists -->
                        <div class="p-4 chat-message-list chat-group-list" data-simplebar>
                            {{-- Start All User List --}}
                            <div>
                                <div class="p-3 fw-bold text-warning">
                                    Pending Friend
                                </div>
                                <ul class="list-unstyled contact-list custom-scrollbar"
                                    style="overflow-y:scroll ; max-height: 200px;">

                                    @forelse ($pending_requests as $item)
                                        <li>
                                            <div class="d-flex align-items-center">
                                                <div class="me-3 ms-0">
                                                    <img src="{{ $item->photo == 'user.png' ? asset("images/users/$item->photo") : asset("images/profile/$item->photo") }}"
                                                        class="rounded-circle avatar-xs" style="object-fit: cover"
                                                        alt="">
                                                </div>
                                                <div class="flex-grow-1">
                                                    <h5 class="font-size-14 m-0">{{ $item->name }}</h5>
                                                </div>

                                                <a href="javascript:void(0)"
                                                    wire:click="cancelAddFriend({{ $item->id }})">
                                                    <i class="ri-user-unfollow-line float-end text-danger"></i>
                                                </a>
                                            </div>
                                        </li>
                                    @empty
                                        <li class="d-flex align-items-center justify-content-center p-0">
                                            <h6 class="text-dark text-center my-4">No Pending Request Found</h6>
                                        </li>
                                    @endforelse
                                </ul>

                            </div>
                            <!-- End All User list -->

                            <!-- Start Pending User lists -->
                            <div class="mt-3" wire:key>
                                <div class="p-3 fw-bold text-warning">
                                    All User
                                </div>
                                <ul class="list-unstyled contact-list custom-scrollbar"
                                    style="overflow-y:scroll ; max-height: 400px;">

                                    @foreach ($users as $item)
                                        {{-- @dd($item) --}}
                                        <li>
                                            <div class="d-flex align-items-center">
                                                <div class="me-3 ms-0">
                                                    <img src="{{ $item->photo == 'user.png' ? asset("images/users/$item->photo") : asset("images/profile/$item->photo") }}"
                                                        class="rounded-circle avatar-xs" style="object-fit: cover"
                                                        alt="">
                                                </div>
                                                <div class="flex-grow-1">
                                                    <h5 class="font-size-14 m-0">{{ $item->name }}</h5>
                                                </div>

                                                <a href="#" wire:click="addFriend({{ $item->id }})">
                                                    <i class="ri-user-add-line float-end text-success"></i>
                                                </a>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <!-- End Pending User lists -->
                        </div>
                        <!-- End All User lists -->



                    </div>
                    <!-- Start Contact content -->
                </div>
                <!-- End User tab-pane -->
            </div>
            <!-- end tab content -->
        </div>
        <!-- end chat-leftsidebar -->

        <div class="w-100">
            <!-- Start User chat -->
            <div class="user-chat w-100 h-100 overflow-hidden" wire:ignore.self>
                <div class="d-lg-flex w-100 h-100">
                    @if ($user_yg_dipilih)
                        <!-- start chat conversation section -->
                        <div class="w-100 overflow-hidden position-relative">
                            <div class="p-3 p-lg-4 border-bottom user-chat-topbar">
                                <div class="row align-items-center">
                                    <div class="col-sm-4 col-8">
                                        <div class="d-flex align-items-center">
                                            <div class="d-block d-lg-none me-2 ms-0">
                                                <a href="javascript: void(0);"
                                                    class="user-chat-remove text-muted font-size-16 p-2"><i
                                                        class="ri-arrow-left-s-line"></i></a>
                                            </div>
                                            <div class="me-3 ms-0">
                                                <img src="{{ $user_yg_dipilih->photo == 'user.png' ? asset("images/users/$user_yg_dipilih->photo") : asset("images/profile/$user_yg_dipilih->photo") }}"
                                                    class="rounded-circle avatar-xs" style="object-fit: cover"
                                                    alt="">
                                            </div>
                                            <div class="flex-grow-1 overflow-hidden">
                                                <h5 class="font-size-16 mb-0 text-truncate"><a href="#"
                                                        class="text-reset user-profile-show">{{ $user_yg_dipilih->name }}</a>
                                                </h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-8 col-4">
                                        <ul class="list-inline user-chat-nav text-end mb-0">
                                            {{-- <li class="list-inline-item">
                                                <div class="dropdown">
                                                    <button class="btn nav-btn dropdown-toggle" type="button"
                                                        data-bs-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false">
                                                        <i class="ri-search-line"></i>
                                                    </button>
                                                    <div class="dropdown-menu p-0 dropdown-menu-end dropdown-menu-md">
                                                        <div class="search-box p-2">
                                                            <input type="text"
                                                                class="form-control bg-light border-0"
                                                                placeholder="Search..">
                                                        </div>
                                                    </div>
                                                </div>
                                            </li> --}}

                                            <li class="list-inline-item">
                                                <div class="dropdown" wire:ignore>
                                                    <button class="btn nav-btn dropdown-toggle" type="button"
                                                        data-bs-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false">
                                                        <i class="ri-more-fill"></i>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-end" wire:ignore.self>
                                                        {{-- <a class="dropdown-item d-block d-lg-none user-profile-show"
                                                            wire:click="gantiTab('profile')"
                                                            {{ $navbar == 'profile' ? ' active' : '' }}
                                                            href="#">View profile <i
                                                                class="ri-user-2-line float-end text-muted"></i></a> --}}

                                                        <a class="dropdown-item delete-all-chats"
                                                            href="javascript:void(0)">Delete All Chat <i
                                                                class="ri-delete-bin-line float-end text-muted"></i></a>
                                                    </div>
                                                </div>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- end chat user head -->

                            <!-- start chat conversation -->
                            <div class="chat-conversation p-3 p-lg-4 custom-scrollbar" id="chat-div"
                                style="overflow-y:scroll">
                                <ul class="list-unstyled mb-0">

                                    @forelse ($chats as $chat)
                                        @php
                                            if ($chat->deleted_for_receiver) {
                                                $chat->content = 'Pesan ini telah dihapus!';
                                            }
                                        @endphp
                                        @if ($chat->sender_id == $user->id)
                                            @if ($chat->deleted_from_sender == 1)
                                                @continue
                                            @endif
                                            @if ($chat->is_file && $chat->deleted_for_receiver != 1)
                                                @if ($chat->mime_type == 'png' || $chat->mime_type == 'jpg')
                                                    <li class="right">
                                                        <div class="conversation-list">
                                                            <div class="user-chat-content">
                                                                <div class="ctext-wrap">

                                                                    <div class="ctext-wrap-content">
                                                                        <div class="card mb-0">

                                                                            <img src="{{ asset("images/chat/$chat->content") }}"
                                                                                class="rounded border w-100 h-100 preview-img"
                                                                                data-bs-target="#imageModal"
                                                                                style="object-fit: cover; cursor: pointer; max-height: 400px;">

                                                                        </div>
                                                                        <div class="message-img-link">
                                                                            <ul class="list-inline mb-0">
                                                                                <li class="list-inline-item">
                                                                                    <a download="{{ $chat->content }}"
                                                                                        href="{{ asset("images/chat/$chat->content") }}"
                                                                                        class="fw-medium">
                                                                                        <i
                                                                                            class="ri-download-2-line text-white"></i>
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                        @if (!$chat->deleted_from_sender)
                                                                            <p class="chat-time mb-0"><i
                                                                                    class="ri-time-line align-middle"></i>
                                                                                <span class="align-middle">
                                                                                    {{ $chat->created_at->format('H:i') }}</span>
                                                                            </p>
                                                                        @endif
                                                                    </div>
                                                                    @if (!$chat->deleted_from_sender)
                                                                        <div class="dropdown align-self-start">
                                                                            <a class="dropdown-toggle" wire:ignore.self
                                                                                href="#" role="button"
                                                                                data-bs-toggle="dropdown"
                                                                                aria-haspopup="true"
                                                                                aria-expanded="false">
                                                                                <i class="ri-more-2-fill"></i>
                                                                            </a>
                                                                            <div class="dropdown-menu"
                                                                                wire:ignore.self>
                                                                                @if ($chat->sender_id == $user->id)
                                                                                    <a class="dropdown-item unsend-msg"
                                                                                        href="#"
                                                                                        wire:key="unsendChat-{{ $chat->id }}"
                                                                                        data-id="{{ $chat->id }}">
                                                                                        Unsend <i
                                                                                            class="ri-delete-bin-line float-end text-muted"></i>
                                                                                    </a>
                                                                                @endif
                                                                                <a class="dropdown-item delete-msg "
                                                                                    href="#"
                                                                                    wire:key="deleteChat-{{ $chat->id }}"
                                                                                    data-id="{{ $chat->id }}">
                                                                                    Delete <i
                                                                                        class="ri-delete-bin-line float-end text-muted"></i>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                @elseif ($chat->mime_type == 'mp4')
                                                    <li class="right">
                                                        <div class="conversation-list">
                                                            <div class="user-chat-content">
                                                                <div class="ctext-wrap">

                                                                    <div class="ctext-wrap-content">
                                                                        <div class="mb-0">
                                                                            <i class="fa-solid fa-video position-absolute ms-2 mt-2 fs-5"
                                                                                style="color: white;"></i>
                                                                            <video
                                                                                class="rounded border w-100 h-100 preview-vid"
                                                                                data-bs-target="#videoModal" nocontrols
                                                                                style="object-fit: cover; cursor: pointer; max-height: 400px;">
                                                                                <source
                                                                                    src="{{ asset("video/chat/$chat->content") }}">
                                                                                Your browser does not support HTML
                                                                                video.

                                                                            </video>

                                                                            <div class="message-img-link">
                                                                                <ul class="list-inline mb-0">
                                                                                    <li class="list-inline-item">
                                                                                        <a download="{{ $chat->content }}"
                                                                                            href="{{ asset("video/chat/$chat->content") }}"
                                                                                            class="fw-medium">
                                                                                            <i
                                                                                                class="ri-download-2-line text-white"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>

                                                                        @if (!$chat->deleted_from_sender)
                                                                            <p class="chat-time mb-0"><i
                                                                                    class="ri-time-line align-middle"></i>
                                                                                <span class="align-middle">
                                                                                    {{ $chat->created_at->format('H:i') }}</span>
                                                                            </p>
                                                                        @endif
                                                                    </div>
                                                                    @if (!$chat->deleted_from_sender)
                                                                        <div class="dropdown align-self-start"
                                                                            wire:ignore>
                                                                            <a class="dropdown-toggle" href="#"
                                                                                role="button"
                                                                                data-bs-toggle="dropdown"
                                                                                aria-haspopup="true"
                                                                                aria-expanded="false">
                                                                                <i class="ri-more-2-fill"></i>
                                                                            </a>
                                                                            <div class="dropdown-menu"
                                                                                wire:ignore.self>
                                                                                @if ($chat->sender_id == $user->id)
                                                                                    <a class="dropdown-item unsend-msg"
                                                                                        href="#"
                                                                                        data-id="{{ $chat->id }}">
                                                                                        Unsend <i
                                                                                            class="ri-delete-bin-line float-end text-muted"></i>
                                                                                    </a>
                                                                                @endif
                                                                                <a class="dropdown-item delete-msg"
                                                                                    href="#"
                                                                                    data-id="{{ $chat->id }}">
                                                                                    Delete <i
                                                                                        class="ri-delete-bin-line float-end text-muted"></i>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                @else
                                                    <li class="right">
                                                        <div class="conversation-list">

                                                            <div class="user-chat-content">
                                                                <div class="ctext-wrap">

                                                                    <div class="ctext-wrap-content">
                                                                        <div class="card p-2 mb-2">
                                                                            <div
                                                                                class="d-flex align-items-center attached-file">
                                                                                <div
                                                                                    class="avatar-sm me-3 ms-0 attached-file-avatar">
                                                                                    <div
                                                                                        class="avatar-title bg-soft-primary text-primary rounded font-size-20">
                                                                                        <i
                                                                                            class="ri-file-text-fill"></i>
                                                                                    </div>
                                                                                </div>
                                                                                <div
                                                                                    class="flex-grow-1 overflow-hidden">
                                                                                    <div class="text-start">
                                                                                        <h5
                                                                                            class="font-size-14 text-truncate mb-1">
                                                                                            {{ $chat->content }}</h5>
                                                                                        <p
                                                                                            class="text-muted text-truncate font-size-13 mb-0">
                                                                                            {{ $chat->file_size >= 1000000 ? round($chat->file_size / 1000000) . ' MB' : ($chat->file_size >= 1000 ? round($chat->file_size / 1000) . ' KB' : $chat->file_size . ' Byte') }}
                                                                                            &nbsp;|&nbsp;
                                                                                            {{ strtoupper($chat->mime_type) }}
                                                                                        </p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="ms-4 me-0">
                                                                                    <div
                                                                                        class="d-flex gap-2 font-size-20 d-flex align-items-start">
                                                                                        <div>
                                                                                            <a href="{{ asset("document/chat/$chat->content") }}"
                                                                                                download="{{ $chat->content }}"
                                                                                                class="fw-medium"
                                                                                                data-bs-toggle="tooltip"
                                                                                                data-bs-title="{{ $chat->content }}">
                                                                                                <i
                                                                                                    class="ri-download-2-line text-black"></i>
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        @if (!$chat->deleted_from_sender)
                                                                            <p class="chat-time mb-0"><i
                                                                                    class="ri-time-line align-middle"></i>
                                                                                <span class="align-middle">
                                                                                    {{ $chat->created_at->format('H:i') }}</span>
                                                                            </p>
                                                                        @endif
                                                                    </div>
                                                                    @if (!$chat->deleted_from_sender)
                                                                        <div class="dropdown align-self-start"
                                                                            wire:ignore>
                                                                            <a class="dropdown-toggle" href="#"
                                                                                role="button"
                                                                                data-bs-toggle="dropdown"
                                                                                aria-haspopup="true"
                                                                                aria-expanded="false">
                                                                                <i class="ri-more-2-fill"></i>
                                                                            </a>
                                                                            <div class="dropdown-menu"
                                                                                wire:ignore.self>
                                                                                @if ($chat->sender_id == $user->id)
                                                                                    <a class="dropdown-item unsend-msg"
                                                                                        href="#"
                                                                                        data-id="{{ $chat->id }}">
                                                                                        Unsend <i
                                                                                            class="ri-delete-bin-line float-end text-muted"></i>
                                                                                    </a>
                                                                                @endif
                                                                                <a class="dropdown-item delete-msg"
                                                                                    href="#"
                                                                                    data-id="{{ $chat->id }}">
                                                                                    Delete <i
                                                                                        class="ri-delete-bin-line float-end text-muted"></i>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </li>
                                                @endif
                                            @else
                                                <li class="right">
                                                    <div class="conversation-list">
                                                        <div class="user-chat-content">
                                                            <div class="ctext-wrap">

                                                                <div class="ctext-wrap-content">
                                                                    <p class="mb-0">
                                                                        {{ $chat->content }}
                                                                    </p>
                                                                    @if (!$chat->deleted_from_sender)
                                                                        <p class="chat-time mb-0"><i
                                                                                class="ri-time-line align-middle"></i>
                                                                            <span class="align-middle">
                                                                                {{ $chat->created_at->format('H:i') }}</span>
                                                                        </p>
                                                                    @endif
                                                                </div>
                                                                @if (!$chat->deleted_from_sender)
                                                                    <div class="dropdown align-self-start" wire:ignore>
                                                                        <a class="dropdown-toggle" href="#"
                                                                            role="button" data-bs-toggle="dropdown"
                                                                            aria-haspopup="true"
                                                                            aria-expanded="false">
                                                                            <i class="ri-more-2-fill"></i>
                                                                        </a>
                                                                        <div class="dropdown-menu" wire:ignore.self>
                                                                            @if ($chat->sender_id == $user->id)
                                                                                <a class="dropdown-item unsend-msg"
                                                                                    href="#"
                                                                                    data-id="{{ $chat->id }}">
                                                                                    Unsend <i
                                                                                        class="ri-delete-bin-line float-end text-muted"></i>
                                                                                </a>
                                                                            @endif
                                                                            <a class="dropdown-item delete-msg"
                                                                                href="#"
                                                                                data-id="{{ $chat->id }}">
                                                                                Delete <i
                                                                                    class="ri-delete-bin-line float-end text-muted"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endif
                                        @else
                                            @if ($chat->deleted_from_receiver == 1)
                                                @continue
                                            @endif
                                            @if ($chat->is_file && $chat->deleted_for_receiver != 1)
                                                @if ($chat->mime_type == 'png' || $chat->mime_type == 'jpg')
                                                    <li>
                                                        <div class="conversation-list">
                                                            <div class="user-chat-content">
                                                                <div class="ctext-wrap">

                                                                    <div class="ctext-wrap-content">
                                                                        <div class="card mb-0">

                                                                            <img src="{{ asset("images/chat/$chat->content") }}"
                                                                                class="rounded border w-100 h-100 preview-img"
                                                                                data-bs-target="#imageModal"
                                                                                style="object-fit: cover; cursor: pointer; max-height: 400px;">

                                                                        </div>
                                                                        <div class="message-img-link">
                                                                            <ul
                                                                                class="d-flex list-inline mb-0 justify-content-end">
                                                                                <li class="list-inline-item">
                                                                                    <a download="{{ $chat->content }}"
                                                                                        href="{{ asset("images/chat/$chat->content") }}"
                                                                                        class="fw-medium">
                                                                                        <i
                                                                                            class="ri-download-2-line text-black"></i>
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>


                                                                        @if (!$chat->deleted_for_sender)
                                                                            <p class="chat-time mb-0 ">
                                                                                <i
                                                                                    class="ri-time-line align-middle"></i>
                                                                                <span class="align-middle">
                                                                                    {{ $chat->created_at->format('H:i') }}</span>
                                                                            </p>
                                                                        @endif
                                                                    </div>
                                                                    <div class="dropdown align-self-start" wire:ignore>
                                                                        <a class="dropdown-toggle" href="#"
                                                                            role="button" data-bs-toggle="dropdown"
                                                                            aria-haspopup="true"
                                                                            aria-expanded="false">
                                                                            <i class="ri-more-2-fill"></i>
                                                                        </a>
                                                                        <div class="dropdown-menu">
                                                                            @if ($chat->sender_id == $user->id)
                                                                                <a class="dropdown-item unsend-msg"
                                                                                    href="#"
                                                                                    data-id="{{ $chat->id }}">
                                                                                    Unsend <i
                                                                                        class="ri-delete-bin-line float-end text-muted"></i>
                                                                                </a>
                                                                            @endif
                                                                            <a class="dropdown-item delete-msg"
                                                                                href="#"
                                                                                data-id="{{ $chat->id }}">
                                                                                Delete <i
                                                                                    class="ri-delete-bin-line float-end text-muted"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                @elseif ($chat->mime_type == 'mp4')
                                                    <li>
                                                        <div class="conversation-list">
                                                            <div class="user-chat-content">
                                                                <div class="ctext-wrap">

                                                                    <div class="ctext-wrap-content">
                                                                        <div class="card mb-0">

                                                                            <video
                                                                                class="rounded border w-100 h-100 preview-vid"
                                                                                data-bs-target="#videoModal" nocontrols
                                                                                style="object-fit: cover; cursor: pointer; max-height: 400px;">
                                                                                <source
                                                                                    src="{{ asset("video/chat/$chat->content") }}">
                                                                                Your browser does not support HTML
                                                                                video.
                                                                            </video>

                                                                        </div>
                                                                        <div class="message-img-link">
                                                                            <ul class="list-inline mb-0">
                                                                                <li class="list-inline-item">
                                                                                    <a download="{{ $chat->content }}"
                                                                                        href="{{ asset("images/chat/$chat->content") }}"
                                                                                        class="fw-medium">
                                                                                        <i
                                                                                            class="ri-download-2-line text-white"></i>
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>

                                                                        @if (!$chat->deleted_from_sender)
                                                                            <p class="chat-time mb-0">
                                                                                <i
                                                                                    class="ri-time-line align-middle"></i>
                                                                                <span
                                                                                    class="align-middle">{{ $chat->created_at->format('H:i') }}</span>
                                                                            </p>
                                                                        @endif
                                                                    </div>

                                                                    <div class="dropdown align-self-start" wire:ignore>
                                                                        <a class="dropdown-toggle" href="#"
                                                                            role="button" data-bs-toggle="dropdown"
                                                                            aria-haspopup="true"
                                                                            aria-expanded="false">
                                                                            <i class="ri-more-2-fill"></i>
                                                                        </a>
                                                                        <div class="dropdown-menu" wire:ignore.self>
                                                                            @if ($chat->sender_id == $user->id)
                                                                                <a class="dropdown-item unsend-msg"
                                                                                    href="#"
                                                                                    data-id="{{ $chat->id }}">
                                                                                    Unsend <i
                                                                                        class="ri-delete-bin-line float-end text-muted"></i>
                                                                                </a>
                                                                            @endif
                                                                            <a class="dropdown-item delete-msg"
                                                                                href="#"
                                                                                data-id="{{ $chat->id }}">
                                                                                Delete <i
                                                                                    class="ri-delete-bin-line float-end text-muted"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                @else
                                                    <li>
                                                        <div class="conversation-list">
                                                            <div class="user-chat-content">
                                                                <div class="ctext-wrap">

                                                                    <div class="ctext-wrap-content">
                                                                        <div class="card p-2 mb-2">
                                                                            <div
                                                                                class="d-flex flex-wrap align-items-center attached-file">
                                                                                <div
                                                                                    class="avatar-sm me-3 ms-0 attached-file-avatar">
                                                                                    <div
                                                                                        class="avatar-title bg-soft-primary text-primary rounded font-size-20">
                                                                                        <i
                                                                                            class="ri-file-text-fill"></i>
                                                                                    </div>
                                                                                </div>
                                                                                <div
                                                                                    class="flex-grow-1 overflow-hidden">
                                                                                    <div class="text-start">
                                                                                        <h5
                                                                                            class="font-size-14 text-truncate mb-1">
                                                                                            {{ $chat->content }}</h5>
                                                                                        <p
                                                                                            class="text-muted text-truncate font-size-13 mb-0">
                                                                                            {{ $chat->file_size >= 1000000 ? round($chat->file_size / 1000000) . ' MB' : ($chat->file_size >= 1000 ? round($chat->file_size / 1000) . ' KB' : $chat->file_size . ' Byte') }}
                                                                                            &nbsp;|&nbsp;
                                                                                            {{ strtoupper($chat->mime_type) }}
                                                                                        </p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="ms-4 me-0">
                                                                                    <div
                                                                                        class="d-flex gap-2 font-size-20 d-flex align-items-start">
                                                                                        <div>
                                                                                            <a href="{{ asset("document/chat/$chat->content") }}"
                                                                                                download="{{ $chat->content }}"
                                                                                                class="fw-medium"
                                                                                                data-bs-toggle="tooltip"
                                                                                                data-bs-title="{{ $chat->content }}">
                                                                                                <i
                                                                                                    class="ri-download-2-line text-white"></i>
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        @if (!$chat->deleted_from_sender)
                                                                            <p class="chat-time mb-0">
                                                                                <i
                                                                                    class="ri-time-line align-middle"></i>
                                                                                <span class="align-middle">
                                                                                    {{ $chat->created_at->format('H:i') }}</span>
                                                                            </p>
                                                                        @endif
                                                                    </div>
                                                                    @if (!$chat->deleted_from_sender)
                                                                        <div class="dropdown align-self-start"
                                                                            wire:ignore>
                                                                            <a class="dropdown-toggle" href="#"
                                                                                role="button"
                                                                                data-bs-toggle="dropdown"
                                                                                aria-haspopup="true"
                                                                                aria-expanded="false">
                                                                                <i class="ri-more-2-fill"></i>
                                                                            </a>
                                                                            <div class="dropdown-menu"
                                                                                wire:ignore.self>
                                                                                @if ($chat->sender_id == $user->id)
                                                                                    <a class="dropdown-item unsend-msg"
                                                                                        href="#"
                                                                                        data-id="{{ $chat->id }}">
                                                                                        Unsend <i
                                                                                            class="ri-delete-bin-line float-end text-muted"></i>
                                                                                    </a>
                                                                                @endif
                                                                                <a class="dropdown-item delete-msg"
                                                                                    href="#"
                                                                                    data-id="{{ $chat->id }}">
                                                                                    Delete <i
                                                                                        class="ri-delete-bin-line float-end text-muted"></i>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endif
                                            @else
                                                <li>
                                                    <div class="conversation-list">
                                                        <div class="user-chat-content">
                                                            <div class="ctext-wrap">

                                                                <div class="ctext-wrap-content">
                                                                    <p class="mb-0">
                                                                        {{ $chat->content }}
                                                                    </p>
                                                                    @if (!$chat->deleted_from_sender)
                                                                        <p class="chat-time mb-0"><i
                                                                                class="ri-time-line align-middle"></i>
                                                                            <span class="align-middle">
                                                                                {{ $chat->created_at->format('H:i') }}</span>
                                                                        </p>
                                                                    @endif
                                                                </div>

                                                                @if (!$chat->deleted_from_sender)
                                                                    <div class="dropdown align-self-start" wire:ignore>
                                                                        <a class="dropdown-toggle" href="#"
                                                                            role="button" data-bs-toggle="dropdown"
                                                                            aria-haspopup="true"
                                                                            aria-expanded="false">
                                                                            <i class="ri-more-2-fill"></i>
                                                                        </a>
                                                                        <div class="dropdown-menu" wire:ignore.self>
                                                                            @if ($chat->sender_id == $user->id)
                                                                                <a class="dropdown-item unsend-msg"
                                                                                    href="#"
                                                                                    data-id="{{ $chat->id }}">
                                                                                    Unsend <i
                                                                                        class="ri-delete-bin-line float-end text-muted"></i>
                                                                                </a>
                                                                            @endif
                                                                            <a class="dropdown-item delete-msg"
                                                                                href="#"
                                                                                data-id="{{ $chat->id }}">
                                                                                Delete <i
                                                                                    class="ri-delete-bin-line float-end text-muted"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endif
                                        @endif
                                    @empty
                                        <div class="text-center my-4 text-secondary">
                                            You don't have a chat history with this person.
                                        </div>
                                    @endforelse


                                </ul>
                            </div>
                            <!-- end chat conversation end -->

                            <!-- start chat input section -->
                            <div class="position-relative">
                                <div class="chat-input-section p-3 p-lg-4 border-top mb-0">

                                    <div class="row g-0 align-items-center">
                                        <div class="file_Upload"></div>

                                        @if ($isFriend == true)
                                            @if (is_null($user_yg_dipilih->deleted_at))
                                                @if (!$is_friend_blocked)
                                                    <form action="" method="post" class="input-group"
                                                        wire:submit.prevent="submitChat()">
                                                        <div class="col">
                                                            <div class="position-relative">
                                                                <div class="chat-input-feedback">
                                                                    <input type="text"
                                                                        class="form-control form-control-lg bg-light border-light"
                                                                        wire:model="chat_input" id=""
                                                                        placeholder="Enter Message..."
                                                                        style="resize: none;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <div class="chat-input-links ms-md-2 me-md-0">
                                                                {{-- <form action="#" wire:submit.prevent="submitChat()"
                                                        method="post"> --}}
                                                                <ul class="list-inline mb-0">

                                                                    <li class="list-inline-item"
                                                                        data-bs-toggle="tooltip"
                                                                        data-bs-trigger="hover"
                                                                        data-bs-placement="top" title="Attached File">
                                                                        <a href="javascript:void(0)"
                                                                            onclick="this.nextElementSibling.click()">
                                                                            <button type="button"
                                                                                class="btn btn-link text-decoration-none font-size-16 btn-lg">
                                                                                <i class="ri-attachment-line"></i>


                                                                            </button>
                                                                        </a>
                                                                        <input class="form-control d-none"
                                                                            type="file" id="mediaChat"
                                                                            wire:model="media_chat">
                                                                    </li>

                                                                    <li class="list-inline-item">
                                                                        <button type="submit"
                                                                            class="btn btn-warning font-size-16 btn-lg chat-send waves-effect waves-light">
                                                                            <i class="ri-send-plane-2-fill"
                                                                                id="button-addon2"></i>
                                                                        </button>
                                                                    </li>
                                                                </ul>
                                                                {{-- </form> --}}
                                                            </div>
                                                        </div>
                                                    </form>
                                                @else
                                                    <div class="text-center">
                                                        <p class="my-2 text-dark">You're blocked this person, unblock
                                                            first to start chatting!</p>
                                                    </div>
                                                @endif
                                            @else
                                                <div class="text-center">
                                                    <p class="my-2 text-dark">Sorry, This account was no longer exist.
                                                        You can't send message to this person again!</p>
                                                </div>
                                            @endif
                                        @else
                                            <div class="text-center">
                                                <p class="my-2 text-dark">You must be friend first to exchange
                                                    messages with this person.</p>
                                            </div>
                                        @endif
                                    </div>



                                </div>
                            </div>
                            <!-- end chat input section -->
                        </div>
                        <!-- end chat conversation section -->

                        <!-- start User profile detail sidebar -->
                        <div class="user-profile-sidebar" wire:ignore.self>
                            <div class="px-3 px-lg-4 pt-3 pt-lg-4">
                                <div class="user-chat-nav text-end">
                                    <button type="button" class="btn nav-btn" id="user-profile-hide">
                                        <i class="ri-close-line"></i>
                                    </button>
                                </div>
                            </div>

                            <div class="text-center p-4 border-bottom">
                                <div class="position-relative mb-4">
                                    {{-- <a href="javascript::void(0)" data-bs-toggle="modal" data-bs-target="#imageModal"
                                        data-bs-image="{{ $user_yg_dipilih->photo == 'user.png' ? asset("images/users/$user_yg_dipilih->photo") : asset("images/profile/$user_yg_dipilih->photo") }}"
                                        class="h-100 w-100">
                                        <img src="{{ $user_yg_dipilih->photo == 'user.png' ? asset("images/users/$user_yg_dipilih->photo") : asset("images/profile/$user_yg_dipilih->photo") }}"
                                            class="rounded-circle avatar-lg" style="object-fit: cover"
                                            alt="">

                                    </a> --}}
                                    <img src="{{ $user_yg_dipilih->photo == 'user.png' ? asset("images/users/$user_yg_dipilih->photo") : asset("images/profile/$user_yg_dipilih->photo") }}"
                                        class="rounded-circle avatar-lg preview-img"
                                        style="object-fit: cover; cursor: pointer;" alt=""
                                        data-bs-target="#imageModal">
                                </div>

                                <h5 class="font-size-16 mb-1 text-truncate">{{ $user_yg_dipilih->name }}
                                    {!! $user->gender == 'male'
                                        ? '<i class="fa-solid fa-mars fs-5 text-primary"></i>'
                                        : '<i class="fa-solid fa-venus fs-5" style="color: #e0276a;"></i>' !!}
                                </h5>
                                <p class="text-muted text-truncate mb-1">
                                    @ {{ $user_yg_dipilih->username }}
                                </p>
                            </div>
                            <!-- End profile user -->

                            <!-- Start user-profile-desc -->
                            <div class="p-4 user-profile-desc" data-simplebar>
                                <div class="text-muted">
                                    <p class="mb-4">
                                        {{ $user_yg_dipilih->bio != null ? $user_yg_dipilih->bio : 'Bio Default' }}
                                    </p>
                                </div>

                                <div class="accordion" id="myprofile">

                                    <div class="accordion-item card border mb-2">
                                        <div class="accordion-header" wire:ignore id="about3">
                                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                data-bs-target="#aboutprofile" aria-expanded="true"
                                                aria-controls="aboutprofile">
                                                <h5 class="font-size-14 m-0">
                                                    <i class="ri-user-2-line me-2 ms-0 align-middle d-inline-block">
                                                    </i>About
                                                </h5>
                                            </button>
                                        </div>
                                        <div id="aboutprofile" wire:ignore.self
                                            class="accordion-collapse collapse show" aria-labelledby="about3"
                                            data-bs-parent="#myprofile">
                                            <div class="accordion-body custom-scrollbar"
                                                style="overflow-y:scroll ; max-height: 200px;">
                                                <div>
                                                    <p class="text-muted mb-1">Full Name</p>
                                                    <h5 class="font-size-14">{{ $user_yg_dipilih->name }}</h5>
                                                </div>

                                                <div class="mt-4">
                                                    <p class="text-muted mb-1">Email</p>
                                                    <h5 class="font-size-14">{{ $user_yg_dipilih->email }}</h5>
                                                </div>

                                                <div class="mt-4">
                                                    <p class="text-muted mb-1">Phone</p>
                                                    <h5 class="font-size-14">
                                                        {{ $user_yg_dipilih->phone != null ? $user_yg_dipilih->phone : '-' }}
                                                    </h5>
                                                </div>

                                                <div class="mt-4">
                                                    <p class="text-muted mb-1">Location</p>
                                                    <h5 class="font-size-14 mb-0">
                                                        {{ $user_yg_dipilih->location != null ? $user_yg_dipilih->location : '-' }}
                                                    </h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="accordion-item card border mb-2">
                                        <div class="accordion-header" wire:ignore id="attachfile3">
                                            <button class="accordion-button collapsed" type="button"
                                                data-bs-toggle="collapse" data-bs-target="#attachprofile"
                                                aria-expanded="false" aria-controls="attachprofile">
                                                <h5 class="font-size-14 m-0">
                                                    <i
                                                        class="ri-attachment-line me-2 ms-0 align-middle d-inline-block">

                                                    </i>
                                                    Files
                                                </h5>
                                            </button>
                                        </div>
                                        <div id="attachprofile" wire:ignore.self class="accordion-collapse collapse"
                                            aria-labelledby="attachfile3" data-bs-parent="#myprofile">
                                            <div class="accordion-body custom-scrollbar"
                                                style="overflow-y:scroll; max-height: 200px">

                                                {{-- START FILE DOCUMENT --}}
                                                @php
                                                    $docs = [];
                                                    foreach ($chats as $item) {
                                                        if ($item->is_file && ($item->mime_type != 'png' && $item->mime_type != 'jpg' && $item->mime_type != 'mp4')) {
                                                            array_push($docs, $item);
                                                        }
                                                    }
                                                @endphp
                                                @forelse ($docs as $item)
                                                    @if ($item->deleted_for_receiver == 1)
                                                        @continue
                                                    @else
                                                        @if ($item->sender_id == $user->id)
                                                            @if ($item->deleted_from_sender == 1)
                                                                @continue
                                                            @else
                                                                <div class="card p-2 border mb-1">

                                                                    <div class="d-flex align-items-center">

                                                                        <div class="avatar-sm me-3 ms-0">
                                                                            <div
                                                                                class="avatar-title bg-soft-warning text-warning rounded font-size-20">
                                                                                <i class="ri-file-text-fill"></i>
                                                                            </div>
                                                                        </div>
                                                                        <div class="flex-grow-1 flex-fill">
                                                                            <div class="text-start">
                                                                                <h5 class="font-size-14 text-truncate mb-1"
                                                                                    style="max-width: 100px">
                                                                                    {{ $item->content }}</h5>
                                                                                <p
                                                                                    class="text-muted text-truncate font-size-13 mb-0">
                                                                                    {{ $item->file_size >= 1000000 ? round($item->file_size / 1000000) . ' MB' : ($item->file_size >= 1000 ? round($item->file_size / 1000) . ' KB' : $item->file_size . ' Byte') }}
                                                                                    &nbsp;|&nbsp;
                                                                                    {{ strtoupper($item->mime_type) }}
                                                                                </p>
                                                                            </div>
                                                                        </div>

                                                                        <div class="ms-4 me-0 ">
                                                                            <ul class="list-inline mb-0 font-size-18">

                                                                                <li class="list-inline-item dropdown"
                                                                                    wire:ignore>
                                                                                    <a class="dropdown-toggle text-muted px-1"
                                                                                        href="#" role="button"
                                                                                        data-bs-toggle="dropdown"
                                                                                        aria-haspopup="true"
                                                                                        aria-expanded="false">
                                                                                        <i class="ri-more-fill"></i>
                                                                                    </a>
                                                                                    <div class="dropdown-menu text-muted px-1"
                                                                                        wire:key="{{ $item->id }}">
                                                                                        <a class="dropdown-item"
                                                                                            href="{{ asset("doc/chat/$item->content") }}"
                                                                                            download="{{ $item->content }}"
                                                                                            data-bs-toggle="tooltip"
                                                                                            data-bs-title="{{ $item->content }}">
                                                                                            Download<i
                                                                                                class="ri-download-2-line float-end text-black"></i>
                                                                                        </a>
                                                                                        <a class="dropdown-item delete-msg"
                                                                                            href="#"
                                                                                            data-id="{{ $item->id }}">
                                                                                            Delete <i
                                                                                                class="ri-delete-bin-line float-end text-muted"></i>
                                                                                        </a>

                                                                                    </div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @else
                                                            @if ($item->deleted_from_receiver == 1)
                                                                @continue
                                                            @else
                                                                <div class="d-flex align-items-center">
                                                                    <a href="{{ asset("doc/chat/$item->content") }}"
                                                                        download="{{ $item->content }}"
                                                                        class="text-decoration-none text-dark mw-100"
                                                                        data-bs-toggle="tooltip"
                                                                        data-bs-title="{{ $item->content }}">
                                                                        <div class="avatar-sm me-3 ms-0">
                                                                            <div
                                                                                class="avatar-title bg-soft-warning text-warning rounded font-size-20">
                                                                                <i class="ri-file-text-fill"></i>
                                                                            </div>
                                                                        </div>
                                                                        <div class="flex-grow-1 flex-fill">
                                                                            <div class="text-start">
                                                                                <h5 class="font-size-14 text-truncate mb-1"
                                                                                    style="max-width: 100px">
                                                                                    {{ $item->content }}</h5>
                                                                                <p
                                                                                    class="text-muted text-truncate font-size-13 mb-0">
                                                                                    {{ $item->file_size >= 1000000 ? round($item->file_size / 1000000) . ' MB' : ($item->file_size >= 1000 ? round($item->file_size / 1000) . ' KB' : $item->file_size . ' Byte') }}
                                                                                    &nbsp;|&nbsp;
                                                                                    {{ strtoupper($item->mime_type) }}
                                                                                </p>
                                                                            </div>
                                                                        </div>

                                                                        <div class="ms-4 me-0 ">
                                                                            <ul class="list-inline mb-0 font-size-18">
                                                                                <li class="list-inline-item">

                                                                                </li>
                                                                                <li class="list-inline-item dropdown"
                                                                                    wire:ignore>
                                                                                    <a class="dropdown-toggle text-muted px-1"
                                                                                        href="#"
                                                                                        role="button"
                                                                                        data-bs-toggle="dropdown"
                                                                                        aria-haspopup="true"
                                                                                        aria-expanded="false">
                                                                                        <i class="ri-more-fill"></i>
                                                                                    </a>
                                                                                    <div class="dropdown-menu"
                                                                                        wire:key="{{ $item->id }}">
                                                                                        <a class="dropdown-item "
                                                                                            href="{{ asset("doc/chat/$item->content") }}"
                                                                                            download="{{ $item->content }}"
                                                                                            data-bs-toggle="tooltip"
                                                                                            data-bs-title="{{ $item->content }}">
                                                                                            Download<i
                                                                                                class="ri-download-2-line float-end text-black"></i>
                                                                                        </a>
                                                                                        <a class="dropdown-item delete-msg"
                                                                                            href="#"
                                                                                            data-id="{{ $item->id }}">
                                                                                            Delete <i
                                                                                                class="ri-delete-bin-line float-end text-muted"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            @endif
                                                        @endif
                                                    @endif
                                                @empty
                                                    <h6 class="py-5 text-dark text-center w-100">
                                                        You doesn't have any document yet
                                                    </h6>
                                                @endforelse
                                                {{-- END FILE DOCUMENT --}}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="accordion-item card border">
                                        <div class="accordion-header" wire:ignore id="media3">
                                            <button class="accordion-button collapsed" type="button"
                                                data-bs-toggle="collapse" data-bs-target="#media"
                                                aria-expanded="false" aria-controls="media">
                                                <h5 class="font-size-14 m-0">
                                                    <i
                                                        class="ri-attachment-line me-2 ms-0 align-middle d-inline-block">
                                                    </i>
                                                    Media
                                                </h5>
                                            </button>
                                        </div>
                                        <div id="media" wire:ignore.self class="accordion-collapse collapse"
                                            aria-labelledby="media" data-bs-parent="#myprofile">
                                            <div class="accordion-body custom-scrollbar"
                                                style="overflow-y:scroll ; max-height: 200px;">

                                                {{-- START PHOTO MEDIA --}}
                                                @php
                                                    $medias = [];
                                                    foreach ($chats as $item) {
                                                        if ($item->is_file && ($item->mime_type == 'png' || $item->mime_type == 'jpg' || $item->mime_type == 'mp4') && $item->deleted_for_receiver != 1 && $item->deleted_from_sender != 1) {
                                                            array_push($medias, $item);
                                                        }
                                                    }
                                                @endphp
                                                @forelse ($medias as $item)
                                                    @if ($item->deleted_for_receiver == 1)
                                                        @continue
                                                    @else
                                                        @if ($item->sender_id == $user->id)
                                                            @if ($item->deleted_from_sender == 1)
                                                                @continue
                                                            @else
                                                                <div class="card p-2 border mb-1">
                                                                    <div class="d-flex align-items-center w-100">


                                                                        <div
                                                                            class="avatar-sm me-3 ms-0  position-relative">


                                                                            {{-- <div class=" overflow-hidden"> --}}
                                                                            @if ($item->mime_type == 'mp4')
                                                                                <div
                                                                                    class="position-absolute top-50 start-50 translate-middle">
                                                                                    <i class="fa-solid fa-video "
                                                                                        style="color: white;"></i>
                                                                                </div>
                                                                                <video class="w-100 h-100 preview-vid"
                                                                                    style="object-fit: cover; cursor: pointer;"
                                                                                    data-bs-target="#videoModal"
                                                                                    nocontrols>
                                                                                    <source
                                                                                        src="{{ asset("video/chat/$item->content") }}">
                                                                                    Your browser does not support HTML
                                                                                    video.
                                                                                </video>
                                                                            @else
                                                                                <img src="{{ asset("images/chat/$item->content") }}"
                                                                                    alt="img"
                                                                                    class="w-100 h-100 preview-img"
                                                                                    style="object-fit: cover; cursor: pointer;"
                                                                                    data-bs-target="#imageModal">
                                                                            @endif
                                                                            {{-- </div> --}}

                                                                        </div>

                                                                        <div class="flex-grow-1 flex-fill">
                                                                            <div class="text-start">
                                                                                <h5 class="font-size-14 mb-1 text-truncate"
                                                                                    style="max-width: 100px;">
                                                                                    {{ $item->content }}</h5>

                                                                            </div>
                                                                        </div>

                                                                        <div class="ms-4 me-0 ">
                                                                            <ul class="list-inline mb-0 font-size-18">
                                                                                <li class="list-inline-item">

                                                                                </li>
                                                                                <li class="list-inline-item dropdown"
                                                                                    wire:ignore>
                                                                                    <a class="dropdown-toggle text-muted px-1"
                                                                                        href="#"
                                                                                        role="button"
                                                                                        data-bs-toggle="dropdown"
                                                                                        aria-haspopup="true"
                                                                                        aria-expanded="false">
                                                                                        <i class="ri-more-fill"></i>
                                                                                    </a>
                                                                                    <div class="dropdown-menu"
                                                                                        wire:key="{{ $item->id }}">

                                                                                        <a class="dropdown-item"
                                                                                            href="{{ asset("doc/chat/$item->content") }}"
                                                                                            download="{{ $item->content }}"
                                                                                            data-bs-toggle="tooltip"
                                                                                            data-bs-title="{{ $item->content }}">
                                                                                            Download<i
                                                                                                class="ri-download-2-line float-end text-black"></i>
                                                                                        </a>
                                                                                        <a class="dropdown-item delete-msg"
                                                                                            href="#"
                                                                                            data-id="{{ $item->id }}">
                                                                                            Delete <i
                                                                                                class="ri-delete-bin-line float-end text-muted"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @else
                                                            @if ($item->deleted_from_receiver == 1)
                                                                @continue
                                                            @else
                                                                <div class="card p-2 border mb-1">
                                                                    <div class="d-flex align-items-center w-100">

                                                                        <div class="avatar-sm me-3 ms-0">
                                                                            {{-- <div class=" overflow-hidden"> --}}
                                                                            @if ($item->mime_type == 'mp4')
                                                                                <video class="w-100 h-100 preview-vid"
                                                                                    style="object-fit: cover; cursor: pointer;"
                                                                                    data-bs-target="#videoModal"
                                                                                    nocontrols>
                                                                                    <source
                                                                                        src="{{ asset("video/chat/$item->content") }}">
                                                                                    Your browser does not support HTML
                                                                                    video.
                                                                                </video>
                                                                            @else
                                                                                <img src="{{ asset("images/chat/$item->content") }}"
                                                                                    alt="img"
                                                                                    class="w-100 h-100 preview-img"
                                                                                    style="object-fit: cover; cursor: pointer;"
                                                                                    data-bs-target="#imageModal">
                                                                            @endif
                                                                            {{-- </div> --}}
                                                                        </div>

                                                                        <div class="flex-grow-1 flex-fill">
                                                                            <div class="text-start">
                                                                                <h5 class="font-size-14 mb-1 text-truncate"
                                                                                    style="max-width: 100px;">
                                                                                    {{ $item->content }}</h5>

                                                                            </div>
                                                                        </div>

                                                                        <div class="ms-4 me-0 ">
                                                                            <ul class="list-inline mb-0 font-size-18">
                                                                                <li class="list-inline-item">

                                                                                </li>
                                                                                <li class="list-inline-item dropdown"
                                                                                    wire:ignore>
                                                                                    <a class="dropdown-toggle text-muted px-1"
                                                                                        href="#"
                                                                                        role="button"
                                                                                        data-bs-toggle="dropdown"
                                                                                        aria-haspopup="true"
                                                                                        aria-expanded="false">
                                                                                        <i class="ri-more-fill"></i>
                                                                                    </a>
                                                                                    <div class="dropdown-menu"
                                                                                        wire:key>

                                                                                        <a class="dropdown-item "
                                                                                            href="{{ asset("doc/chat/$item->content") }}"
                                                                                            download="{{ $item->content }}"
                                                                                            data-bs-toggle="tooltip"
                                                                                            data-bs-title="{{ $item->content }}">
                                                                                            Download<i
                                                                                                class="ri-download-2-line float-end text-black"></i>
                                                                                        </a>

                                                                                        <a class="dropdown-item delete-msg"
                                                                                            href="#"
                                                                                            data-id="{{ $chat->id }}">
                                                                                            Delete <i
                                                                                                class="ri-delete-bin-line float-end text-muted"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @endif
                                                    @endif
                                                @empty
                                                    <h6 class="py-5 text-dark text-center w-100">
                                                        You doesn't have any media yet
                                                    </h6>
                                                @endforelse
                                                {{-- END PHOTO MEDIA --}}
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end profile-user-accordion -->
                                </div>
                                <!-- end user-profile-desc -->
                                @if (!$is_friend_blocked)
                                    <button type="button" class="btn btn-danger btn-sm float-start block-btn"
                                        data-id="{{ $user_yg_dipilih->id }}">
                                        <i class="ri-delete-bin-fill me-1 ms-0 "></i> Block User
                                    </button>
                                @else
                                    <button type="button" class="btn btn-danger btn-sm float-start unblock-btn"
                                        data-id="{{ $user_yg_dipilih->id }}">
                                        <i class="ri-delete-bin-fill me-1 ms-0 "></i> Unblock User
                                    </button>
                                @endif
                            </div>
                            <!-- end User profile detail sidebar -->
                        </div>
                    @else
                        <div
                            class="col d-none d-lg-flex h-100 align-items-center justify-content-center middle-area close-left">
                            <div class="start-chat-area">
                                <div class="d-flex justify-content-center align-items-center mb-3">
                                    <img src="{{ asset('/images/logo.png') }}" alt="" height="180">
                                </div>
                                <p class="text-muted">Please select your friend to Start messaging.</p>
                                <div class="btn btn-lg btn-outline-warning rounded-5 px-5 py-3 fw-semibold d-flex justify-content-center align-items-center"
                                    wire:click="gantiTab('contact')">
                                    Click here
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <!-- End User chat -->

            </div>
            {{-- End User chat --}}

        </div>
        <!-- end  layout wrapper -->
    </div>
    @push('script')
        <script>
            $(document).ready(function() {
                setInterval(() => {
                    @this.refreshChat();
                    console.log('refresh');
                    // console.log(@this.search_gender);
                    // var objDiv = document.getElementById("chat-div");
                    // objDiv.scrollTop = objDiv.scrollHeight;
                }, 2000);
            });

            // $(document).ready(function() {
            //     Livewire.on('closeModal', function() {
            //         $('#editProfile').modal('hide')
            //     })
            // });

            // Start preview modal when image clicked
            const imageModal = document.getElementById('imageModal')
            $('body').on('click', '.preview-img', function(event) {
                let button = event.currentTarget
                let content = button.getAttribute('src')
                console.log(content)
                imageModal.getElementsByTagName("img")[0].src = content
                console.log(imageModal.getElementsByTagName("img")[0])
                const openImageModal = new bootstrap.Modal('#imageModal', {
                    keyboard: false
                })
                openImageModal.show();
            })
            // End preview modal when image clicked

            // Start preview modal when video clicked
            const videoModal = document.getElementById('videoModal')
            $('body').on('click', '.preview-vid', function(event) {
                let button = event.currentTarget
                let content = button.querySelector('source').getAttribute('src')
                console.log(content)
                const modalBodyVideo = videoModal.querySelector('.modal-body video')
                modalBodyVideo.querySelector('source').src = content
                modalBodyVideo.load()
                const openVideoModal = new bootstrap.Modal('#videoModal', {
                    keyboard: false
                })
                openVideoModal.show()
            })

            // Start SWAL when unfriend btn clicked
            $('body').on('click', '.unfriend-btn', function() {
                const name = $(this).data('name');
                const id = $(this).data('id');

                Swal.fire({
                    title: 'Are you sure?',
                    text: "Are you sure you want to unfriend with " + name + "?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Unfriend!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        @this.unFriend(id);
                    }
                })
            });
            // End SWAL when unfriend btn clicked

            // Start SWAL when block btn clicked
            $('body').on('click', '.block-btn', function() {
                const id = $(this).data('id');
                console.log(id);
                Swal.fire({
                    title: 'Are you sure?',
                    text: "Are you sure you want to block this person?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, block it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        @this.blockFriend(id);
                    }
                })
            });
            // End SWAL when block btn clicked

            // Start SWAL when unblock btn clicked
            $('body').on('click', '.unblock-btn', function() {
                const id = $(this).data('id');

                Swal.fire({
                    title: 'Are you sure?',
                    text: "Are you sure you want to unblock this person?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, unblock it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        @this.unblockFriend(id);
                    }
                })
            });
            // End SWAL when block btn clicked

            // Start SWAL when delete account btn clicked
            $('body').on('click', '.delete-account-btn', function() {
                Swal.fire({
                    title: 'Are you sure you want to delete your account?',
                    text: "Deleted data cannot be recovered (including chat history data and friends). Your friend will also not be able to chat with you again!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Delete It!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        @this.deleteAccount();
                    }
                })
            });
            // End SWAL when delete account btn clicked

            // Start SWAL when unsend btn clicked
            $('body').on('click', '.unsend-msg', function() {
                const id = $(this).data('id');

                Swal.fire({
                    title: 'Are you sure',
                    text: "Want to unsend this chat? unsended chat data cannot readed anymore!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Delete It!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        @this.unsendChat(id);
                    }
                });
            });
            // End SWAL when unsend btn clicked

            // Start SWAL when delete message btn clicked
            $('body').on('click', '.delete-msg', function() {
                let id = $(this).data('id');
                console.log($(this).data('id'));
                Swal.fire({
                    title: 'Are you sure',
                    text: "Want to delete this chat? Deleted data cannot be recovered!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Delete It!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        console.log(id);
                        @this.deleteChat(id);
                    }
                });
            });
            // End SWAL when delete message btn clicked

            // Start reset photo profile button
            $('body').on('click', '#resetPhoto', function() {
                Swal.fire({
                    title: 'Are you sure?',
                    text: "Want to remove your current photo? your photo will change to default again!!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Remove It!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        @this.removePhotoProfile();
                        $('#editProfileModal').find('.preview-img-profile').attr("src",
                            "../images/users/user.png");
                    }
                })
            });
            // End reset photo profile button

            // Start SWAL when delete all message btn clicked
            $('body').on('click', '.delete-all-chats', function() {
                Swal.fire({
                    title: 'Are you sure?',
                    text: "Want to delete all chat history with this user? Deleted data cannot be recovered!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Delete It!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        @this.deleteAllChat();
                    }
                })
            });
            // End SWAL when delete all message btn clicked
            $('.modal-video').on('shown.bs.modal', function(e) {
                // $('body').removeClass('modal-open');
                $(this).css('z-index', 1080);
            });
            $('.modal-foto').on('shown.bs.modal', function(e) {
                // $('body').removeClass('modal-open');
                $(this).css('z-index', 1080);
            });
            $('.modal-video').on('hidden.bs.modal', function(e) {
                // $('body').removeClass('modal-open');
                $(this).css('z-index', '-1');
            });
            $('.modal-foto').on('hidden.bs.modal', function(e) {
                // $('body').removeClass('modal-open');
                $(this).css('z-index', '-1');
            });

            // Start livewire event close modal for change password
            Livewire.on("closeModalChangePassword", () => {
                $('#modal-changePassword').modal('hide');
            });
            // End livewire close modal for change password

            // Start livewire event close modal for edit profile
            Livewire.on("closeModalEditProfile", () => {
                $('#modal-editProfile').modal('hide');
            });
            // End livewire close modal for edit profile
        </script>
    @endpush
</div>
