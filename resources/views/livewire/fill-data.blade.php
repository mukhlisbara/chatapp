@extends('layouts.app')

<div class="container">
    <div class="row justify-content-center align-items-center vh-100">
        <div class="col-md-6">
            <div class="w-100 my-5 d-flex justify-content-center align-items-center">
                <div class="rounded-circle neumorph-light p-5">
                    <img src="{{ asset('images/logo.png') }}" alt="Logo Happy Chat" style="height: 13rem; width: auto;">
                </div>
            </div>
            <div class="w-100 my-5">
                <h1 class="text-center mb-0" style="font-size: 5rem;">
                    <span class="text-warning">Thank</span>
                    <span class="text-black">You</span>
                </h1>
                <div class="text-center mt-3">Before you can access our application, kindly check your email inbox
                    to
                    confirm your account. If you didn't receive the email,
                    <form class="d-inline-flex" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0">click here to
                            request
                            another</button>.
                    </form>
                    or just
                    <a class="text-decoration-none text-danger lh-1" href="{{ route('logout') }}"
                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Log out.
                    </a>
                </div>
                @if (session('resent'))
                    <div class="alert alert-success my-4" role="alert">
                        {{ __('A fresh verification link has been sent to your email address.') }}
                    </div>
                @endif
            </div>
        </div>

        <div class="col-6">
            <div class="card card-primary mx-auto" style="max-width: 500px">
                <div class="card-header">Complete Registration

                </div>
                <div class="card-body">
                    <form wire:submit.prevent="fillData">
                        @csrf

                        <div class="form-group">
                            <label for="name" class="col-md-4 col-form-label">Username<span
                                    class="text-danger">*</span></label>

                            <div class="col-md-12" style="max-width: 500px">
                                <input id="username" type="text"
                                    class="form-control @error('username') is-invalid @enderror"
                                    value="{{ old('username') }}" required autocomplete="username" autofocus
                                    wire:model.defer="username" placeholder="Example" wire:change='checkInput'>

                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="gender" class="col-md-4 col-form-label">Gender<span
                                    class="text-danger">*</span></label>
                            <div class="col-md-12">
                                <select class="form-select @error('gender') is-invalid @enderror" name="gender"
                                    id="gender" value="{{ old('gender') }}" required autocomplete="gender"
                                    wire:model.defer="gender">
                                    <option value="male" selected>Male</option>
                                    <option value="female">Female</option>
                                </select>
                                @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-md-4 col-form-label">Phone Number<span
                                    class="text-danger">*</span></label>

                            <div class="col-md-12" style="max-width: 500px">
                                <input id="phone" type="text"
                                    class="form-control @error('phone') is-invalid @enderror"
                                    value="{{ old('phone') }}" required autocomplete="phone" autofocus
                                    wire:model.defer="phone" placeholder="Example" wire:change='checkInput'>

                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="location" class="col-md-4 col-form-label">Location<span
                                    class="text-danger">*</span></label>
                            <div class="col-md-12">
                                <select class="form-select @error('location') is-invalid @enderror" name="location"
                                    id="location" value="{{ old('location') }}" required autocomplete="location"
                                    wire:model.defer="location">
                                    <option value="surabaya" selected>Surabaya</option>
                                    <option value="jakarta">Jakarta</option>
                                </select>
                                @error('location')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 col-form-label">Password<span
                                    class="text-danger">*</span></label>

                            <div class="col-md-12">
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="new-password" wire:model.defer="password"
                                    placeholder="*************">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 col-form-label">Confirm
                                Password<span class="text-danger">*</span></label>

                            <div class="col-md-12">
                                <input id="password-confirm" type="password" class="form-control"
                                    name="password_confirmation" required autocomplete="new-password"
                                    wire:model.defer="password_confirmation" placeholder="*************">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary w-100 mt-2"> Save Data </button>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
    @csrf
</form>
{{-- PWA --}}
<script src="{{ asset('/sw.js') }}"></script>
<script>
    if (!navigator.serviceWorker.controller) {
        navigator.serviceWorker.register("/sw.js").then(function(reg) {
            console.log("Service worker has been registered for scope: " + reg.scope);
        });
    }
</script>



@push('script')
    <script>
        $(document).ready(function() {
            if (@this.is_new) {
                Swal.fire({
                    icon: 'info',
                    title: 'Successfully Registed',
                    html: 'Thank you for your registration, this is your default credential <br>' +
                        '<div class="w-100 text-center my-4 py-4 px-5 rounded-4" style="color: black; background-color: #eff4fd;">' +
                        '<table class="mb-1" style="width:100%; border-none">' +
                        '<tr class="text-start"><td class="fw-semibold">Email</td><td class="fw-semibold pe-2">:</td><td>{{ $user->email }}</td></tr>' +
                        '<tr class="text-start"><td class="fw-semibold">Username</td><td class="fw-semibold pe-2">:</td><td>{{ $user->username }}</td></tr>' +
                        '<tr class="text-start"><td class="fw-semibold">Password</td><td class="fw-semibold pe-2">:</td><td>password</td></tr>' +
                        '</table></div>' +
                        'If you log out before you have finished providing your personal data, use your default credential to log in again. Please save it if you feel it is necessary!!',
                    confirmButtonText: 'Ok, Noted It!!',
                    confirmButtonColor: '#3085d6',
                    customClass: {
                        confirmButton: 'py-3 px-4 rounded-5',
                    }
                    // footer: '<a href="">Why do I have this issue?</a>'
                })
            }
            // Start giving value darkmode from js to php livewire
            darkThemeSelected = localStorage.getItem("darkSwitch") !== null && localStorage.getItem(
                "darkSwitch") === "dark";
            if (darkThemeSelected) {
                Livewire.emit('darkMode', 1)
            } else {
                Livewire.emit('darkMode', 0)
            }

            Livewire.on("darkModeInit", () => {
                darkThemeSelected = localStorage.getItem("darkSwitch") !== null && localStorage.getItem(
                    "darkSwitch") === "dark";
                if (darkThemeSelected) {
                    Livewire.emit('darkMode', 1)
                } else {
                    Livewire.emit('darkMode', 0)
                }
            });

            console.log(window.location.origin + '/home');

            Livewire.on("redirectHome", () => {
                console.log('{{ url()->current() }}');
            });
            // End giving value darkmode from js to php livewire
        });
    </script>
@endpush
