@extends('layouts.app')
{{-- @section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Verify Your Email Address') }}</div>

                    <div class="card-body">
                        @if (session('resent'))
                            <div class="alert alert-success" role="alert">
                                {{ __('A fresh verification link has been sent to your email address.') }}
                            </div>
                        @endif
                        <div
                            class="col d-none d-lg-flex h-100 align-items-center justify-content-center middle-area close-left">
                            <div class="start-chat-area">
                                <div class="d-flex justify-content-center align-items-center mb-3">
                                    <img src="{{ asset('/images/logo.png') }}" alt="" height="180">
                                </div>
                                <p class="text-muted">Please select your friend to Start messaging.</p>

                            </div>
                        </div>
                        {{ __('Before proceeding, please check your email for a verification link.') }}
                        {{ __('If you did not receive the email') }},
                        <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                            @csrf
                            <button type="submit"
                                class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection --}}
@section('content')
    <div class="container">
        <div class="row justify-content-center align-items-center vh-100">
            <div class="col-md-8">
                <div class="w-100 d-flex justify-content-center align-items-center">
                    <div class="rounded-circle neumorph-light p-5">
                        <img src="{{ asset('images/logo.png') }}" alt="Logo Happy Chat" style="height: 16rem; width: auto;">
                    </div>
                </div>
                <div class="w-100">
                    <h1 class="text-center mb-0" style="font-size: 5rem;">
                        <span class="text-warning">Thank</span>
                        <span class="text-black">You</span>
                    </h1>
                    <div class="text-center mt-3">Before you can access our application, kindly check your email inbox to
                        confirm your account. If you didn't receive the email,
                        <form class="d-inline-flex" method="POST" action="{{ route('verification.resend') }}">
                            @csrf
                            <button type="submit" class="btn btn-link p-0 m-0">click here to request another</button>.
                        </form>
                        or just
                        <a class="text-decoration-none text-danger lh-1" href="{{ route('logout') }}"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Log out.
                        </a>
                    </div>
                    @if (session('resent'))
                        <div class="alert alert-success my-4" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif
                </div>
                {{-- <div class="card">
                    <div class="card-header">{{ __('Verify Your Email Address') }}</div>

                    <div class="card-body">
                        @if (session('resent'))
                            <div class="alert alert-success" role="alert">
                                {{ __('A fresh verification link has been sent to your email address.') }}
                            </div>
                        @endif

                        {{ __('Before proceeding, please check your email for a verification link.') }}
                        {{ __('If you did not receive the email') }},
                        <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                            @csrf
                            <button type="submit"
                                class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
                        </form>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
@endsection
