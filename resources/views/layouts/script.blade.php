{{-- Jquerry for Bootstrap CDN --}}
<script src="{{ asset('js/jquery-3.6.1.js') }}"></script>

<!-- JAVASCRIPT -->
{{-- <script src="{{ asset('/libs/jquery/jquery.min.js') }}"></script> --}}
<script src="{{ asset('/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<script src="{{ asset('/libs/simplebar/simplebar.min.js') }}"></script>
<script src="{{ asset('/libs/node-waves/waves.min.js') }}"></script>

<!-- Magnific Popup-->
<script src="{{ asset('/libs/magnific-popup/jquery.magnific-popup.min.js') }}"></script>

<!-- owl.carousel js -->
<script src="{{ asset('/libs/owl.carousel/owl.carousel.min.js') }}"></script>

<!-- page init -->
<script src="{{ asset('/js/pages/index.init.js') }}"></script>

<script src="{{ asset('/js/app.js') }}"></script>

<!-- Swiper JS -->
<script src="{{ asset('/libs/swiper/swiper-bundle.min.js') }}"></script>




{{-- SCRIPT --}}
{{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"></script> --}}
