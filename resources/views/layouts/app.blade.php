<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">


<head>
    <meta charset="utf-8">
    <title>Happy Chat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Responsive Bootstrap 5 Happy Chat" name="description" />
    <meta content="Themesbrand" name="author" />
    @livewireStyles
    @include('layouts.style')
</head>

<body>
    <div id="app">
        <main>
            @yield('content')
        </main>
    </div>


</body>
@livewireScripts
@include('layouts.script')

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<x-livewire-alert::scripts />

@stack('script')

</html>
