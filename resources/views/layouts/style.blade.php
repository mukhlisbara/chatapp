<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ config('app.name', 'Laravel') }}</title>

<!-- Fonts -->
<link rel="dns-prefetch" href="//fonts.gstatic.com">
<link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css">

<!-- Scripts -->
{{-- @vite(['resources/sass/app.scss', 'resources/js/app.js']) --}}

<!-- App favicon -->
<link rel="shortcut icon" href="{{ asset('/images/logo.png') }}">

<!-- magnific-popup css -->
<link href="{{ asset('/libs/magnific-popup/magnific-popup.css') }}" rel="stylesheet" type="text/css" />

<!-- owl.carousel css -->
<link rel="stylesheet" href="{{ asset('/libs/owl.carousel/assets/owl.carousel.min.css') }}">

<link rel="stylesheet" href="{{ asset('/libs/owl.carousel/assets/owl.theme.default.min.css') }}">


<!-- Bootstrap Css -->
<link href="{{ asset('/css/bootstrap.min.css') }}" id="bootstrap-style" rel="stylesheet" type="text/css" />
<!-- Icons Css -->
<link href="{{ asset('/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
<!-- App Css-->
<link href="{{ asset('/css/app.min.css') }}" id="app-style" rel="stylesheet" type="text/css" />

<!-- swiper css -->
<link rel="stylesheet" href="{{ asset('/libs/swiper/swiper-bundle.min.css') }}" />
