<?php

use App\Http\Controllers\GoogleController;
use App\Http\Livewire\Auth\Login;
use App\Http\Livewire\Auth\Register;
use App\Http\Livewire\Dashboard;
use App\Http\Livewire\FillData;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes(['login' => false, 'register' => false, 'verify' => true,]);

route::middleware('guest')->group(function () {
    Route::get('/login', Login::class)->name('login');
    Route::get('/register', Register::class)->name('register');
});

Route::get('/auth/google', [GoogleController::class, 'redirectToGoogle'])->name('google.login');
Route::get('/auth/google/callback', [GoogleController::class, 'handleGoogleCallback'])->name('google.callback');

// route::middleware(['auth', 'verified'])->group(function () {
//     Route::get('/dashboard', Dashboard::class)->name('dashboard');
// });

Route::middleware('auth')->group(function () {
    Route::get('/fill-data/{new?}', FillData::class)->name('fill-data');
    Route::get('/dashboard', Dashboard::class)->middleware(['auth', 'verified'])->name('dashboard');
    Route::get('/verify-edit', function () {
        return view('auth.verify-edit');
    });
});

Route::post('/store-profile', [UserController::class, 'store'])->name('store-profile');

// Route::Livewire('/user/edit/{id}', 'user.edit')->layout('app')->name('user.edit');
