<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'              => 'Mukhlis Bara Pamungkas',
            'phone'             => '081512309121',
            'gender'            => 'male',
            'location'          => 'surabaya',
            'email'             => 'bara@bara.com',
            'email_verified_at' => Carbon::now(),
            'username'          => 'bara',
            'password'          => bcrypt('qweqweqwe'),
        ]);

        User::create([
            'name'              => 'Ferdyansyah Nurrohim',
            'phone'             => '081512309121',
            'gender'            => 'male',
            'location'          => 'surabaya',
            'email'             => 'ferdy@ferdy.com',
            'email_verified_at' => Carbon::now(),
            'username'          => 'ferdy',
            'password'          => bcrypt('qweqweqwe'),
        ]);

        User::create([
            'name'              => 'Denassyah Nurrohman',
            'phone'             => '081512309132',
            'gender'            => 'male',
            'location'          => 'jakarta',
            'email'             => 'denas@denas.com',
            'email_verified_at' => Carbon::now(),
            'username'          => 'denas',
            'password'          => bcrypt('qweqweqwe'),
        ]);


        $datas = [];

        $genders = array(
            'male',
            'female',
        );

        $citys = array(
            'surabaya',
            'jakarta',
        );

        for ($i = 1; $i <= 20; $i++) {
            $gender = array_rand($genders);
            $city = array_rand($citys);
            $datas[] = [
                'name'                => "User$i",
                'phone'               => mt_rand(100000000000, 999999999999),
                'gender'              => $genders[$gender],
                'location'            => $citys[$city],
                'email'               => "user$i@user.com",
                'email_verified_at'   => Carbon::now(),
                'username'            => "user$i",
                'password'            => bcrypt('qweqweqwe'),
            ];
        }

        User::insert($datas);
    }
}
